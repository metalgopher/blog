import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImageFullScreenViewerPageRoutingModule } from './image-full-screen-viewer-routing.module';

import { ImageFullScreenViewerPageComponent } from './image-full-screen-viewer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageFullScreenViewerPageRoutingModule
  ],
  declarations: [ImageFullScreenViewerPageComponent]
})
export class ImageFullScreenViewerPageModule {}
