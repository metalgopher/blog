import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImageFullScreenViewerPage } from './image-full-screen-viewer.page';

describe('ImageFullScreenViewerPage', () => {
  let component: ImageFullScreenViewerPage;
  let fixture: ComponentFixture<ImageFullScreenViewerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageFullScreenViewerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImageFullScreenViewerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
