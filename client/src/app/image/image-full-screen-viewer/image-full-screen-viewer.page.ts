import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-image-full-screen-viewer',
  templateUrl: './image-full-screen-viewer.page.html',
  styleUrls: ['./image-full-screen-viewer.page.scss'],
})
export class ImageFullScreenViewerPageComponent implements OnInit {
  @Input() imgSrc: string;

  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }

  closeImg() {
    this.modalController.dismiss();
  }
}
