import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImageFullScreenViewerPageComponent } from './image-full-screen-viewer.page';

const routes: Routes = [
  {
    path: '',
    component: ImageFullScreenViewerPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImageFullScreenViewerPageRoutingModule {}
