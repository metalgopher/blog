import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TitleBarComponent } from './title-bar.component';

@NgModule({
  declarations: [TitleBarComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    TitleBarComponent
  ]
})
export class TitleBarModule { }
