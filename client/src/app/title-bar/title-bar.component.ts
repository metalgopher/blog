import { Component, OnInit, Input } from '@angular/core';
import { User } from '../models/user';

import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, PopoverController } from '@ionic/angular';
import { LoginPageComponent } from './login/login.page';
import { RegisterPageComponent } from './register/register.page';
import { RegisterSuccessPageComponent } from './register/register-success/register-success.page';
import { AuthService } from '../service/auth.service';
import { CurrentUserPopoverComponent } from './current-user-popover/current-user-popover.component';

@Component({
  selector: 'app-title-bar',
  templateUrl: './title-bar.component.html',
  styleUrls: ['./title-bar.component.scss'],
})
export class TitleBarComponent implements OnInit {
  @Input() pageTitle: string;
  public currentUser: User;
  public doLogin = false;
  public doRegister = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loc: Location,
    private modalController: ModalController,
    private popoverController: PopoverController,
    private authService: AuthService
  ) {
    this.authService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.route.data.forEach(next => {
      if (!this.currentUser) {
        if (next.doLogin) {
          this.doLogin = true;
        }
        if (next.doRegister) {
          this.doRegister = true;
        }
      } else {
        if (next.doLogin || next.doRegister) {
          this.loc.back();
        }
      }
    });

    //open login modal
    if (this.doLogin) {
      this.presentLoginModal();
    }
    
    //open register modal
    if (this.doRegister) {
      this.presentRegisterModal();
    }
  }

  async presentLoginModal() {
    const modal = await this.modalController.create({
      component: LoginPageComponent,
      swipeToClose: true,
      // cssClass: 'my-custom-class'
    });

    modal.onDidDismiss()
      .then(async (result) => {
        //if result was 'register', continue to registration modal flow
        if (result.data) {
          // console.log('login modal dismiss result: ' + JSON.stringify(result));
          switch (result.data) {
            case 'register':
              this.presentRegisterModal();
              break;
            case 'login':
              //see if returnUrl is present
              let returnUrl;
              this.route.queryParams.forEach(next => {
                if (next.returnUrl) {
                  returnUrl = next.returnUrl;
                }
              });

              if (returnUrl) {
                this.router.navigate([returnUrl]);
                break;
              }

              //check if login via route
              if (this.doLogin) {
                this.loc.back();
              }
              break;
          }
        } else {
          //check if login via route
          if (this.doLogin) {
            this.loc.back();
          }
        }
    });

    return await modal.present();
  }

  async presentRegisterModal() {
    const modal = await this.modalController.create({
      component: RegisterPageComponent,
      swipeToClose: true,
      cssClass: 'reg-modal'
    });

    modal.onDidDismiss()
      .then((result) => {
        //if result was 'success', continue to reg success modal
        if (result && result.data === 'success') {
          this.presentRegSuccessModal();
        } else {
          if (this.doLogin || this.doRegister) {
            this.loc.back();
          }
        }
    });

    return await modal.present();
  }

  async presentRegSuccessModal() {
    const modal = await this.modalController.create({
      component: RegisterSuccessPageComponent,
      swipeToClose: true,
      cssClass: 'reg-modal'
    });

    modal.onDidDismiss()
      .then((result) => {
        if (this.doLogin || this.doRegister) {
          this.loc.back();
        }
    });

    return await modal.present();
  }

  async presentCurrentUserPopover(ev) {
    const popover = await this.popoverController.create({
      component: CurrentUserPopoverComponent,
      backdropDismiss: true,
      animated: true,
      event: ev,
      componentProps: {currentUser: this.currentUser}
      // cssClass: 'popover-current-user'
    });

    return await popover.present();
  }
}
