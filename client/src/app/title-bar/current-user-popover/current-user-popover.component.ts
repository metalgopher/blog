import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/service/auth.service';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-current-user-popover',
  templateUrl: './current-user-popover.component.html',
  styleUrls: ['./current-user-popover.component.scss'],
})
export class CurrentUserPopoverComponent implements OnInit {
  @Input() currentUser: User;

  constructor(
    private authService: AuthService,
    private popoverController: PopoverController
  ) { }

  ngOnInit() {
  }

  logout() {
    this.popoverController.dismiss();
    this.authService.logout();
  }
}
