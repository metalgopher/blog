import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { HttpService } from '../../service/http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from '../../service/auth.service';
import { User } from 'src/app/models/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPageComponent implements OnInit {
  public isLoading = false;
  public isVerifying = false;
  public isShowingResendAccountVerification = false;
  public isShowingResendAccountVerificationSuccess = false;
  public requestError1: string;
  public requestError2: string;
  public loginForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private modalController: ModalController, 
    private httpService: HttpService, 
    private authService: AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.email]],
      code: ['', []],
   })
  }

  // enables access to error controls from html
  // get errorControl() {
  //   return this.loginForm.controls;
  // }

  async submitLoginForm() {
    // console.log(this.loginForm.value)
    this.requestError1 = this.requestError2 = '';
    if (!this.loginForm.valid) {
      if (!this.loginForm.get('code').valid) {
        this.requestError1 = 'Invalid auth code.';
        this.requestError2 = '';
      } else {
        this.requestError1 = 'Invalid login.';
        this.requestError2 = '';
      }
      return;
    }

    //handle valid form submission
    let loginResult;
    if (!this.isVerifying) {
      loginResult = await this.sendLoginAuthCode();
      if (loginResult) {
        //change form state to verifying
        this.isVerifying = true;

        //add auth code validator to form control
        this.loginForm.get('code').setValidators(Validators.pattern('[0-9]{7}'));
      }
      return;
    }

    loginResult = await this.verifyLogin();
    if (loginResult) {
      //login successful, dismiss modal
      this.dismissModal('login');
    }
  }

  async sendLoginAuthCode() {
    try {
      this.isLoading = true;
      let response = await this.httpService.postAccountLogin(this.loginForm.get('email').value.toLowerCase());
      this.isLoading = false;
      switch (response.status) {
        case 200:
          //success
          this.requestError1 = this.requestError2 = '';
          break;
        default:
          this.requestError1 = 'Unknown error occurred';
          this.requestError2 = '';
          return false;
      }
    } catch(err) {
      this.isLoading = false;

      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError1 = 'Bad request';
          this.requestError2 = '';
          break;
        case 404:
          this.requestError1 = 'Account not found';
          this.requestError2 = '';
          return false;
        case 409:
          //success
          this.requestError1 = 'Account not verified';
          this.requestError2 = 'Please check your email.';
          this.isShowingResendAccountVerification = true;
          return false;
        default:
          this.requestError1 = 'Unknown error occurred';
          this.requestError2 = '';
          return false;
      }
    }

    //success
    return true;
  }

  async verifyLogin() {
    let response;
    try {
      this.isLoading = true;
      response = await this.httpService.postAccountLoginVerify(
        this.loginForm.get('email').value.toLowerCase(), this.loginForm.get('code').value
      );
      this.isLoading = false;
      switch (response.status) {
        case 200:
          //success
          this.requestError1 = this.requestError2 = '';
          break;
        default:
          this.requestError1 = 'Unknown error occurred';
          this.requestError2 = '';
          return false;
      }
    } catch(err) {
      this.isLoading = false;

      let e = err as HttpErrorResponse;

      //log error
      // console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError1 = 'Bad request';
          this.requestError2 = '';
          return false;
        case 401:
          this.requestError1 = 'Invalid auth.';
          this.requestError2 = 'Please try again.';
          return false;
        default:
          this.requestError1 = 'Unknown error occurred';
          this.requestError2 = '';
          return false;
      }
    }

    // -- success --
    let isReturning = false;
    this.route.queryParams.forEach(next => {
      if (next.returnUrl) {
        isReturning = true;
      }
    });

    //update auth service
    let resUser = response.body as User;
    this.authService.login(resUser, isReturning);
    return true;
  }
  
  async resendVerification() {
    let response;
    try {
      this.isLoading = true;
      response = await this.httpService.postAccountRegisterResendVerification(this.loginForm.get('email').value.toLowerCase());
      this.isLoading = false;
      switch (response.status) {
        case 200:
          //success
          this.requestError1 = this.requestError2 = '';
          this.isShowingResendAccountVerificationSuccess = true;

          //hide resend button
          this.isShowingResendAccountVerification = false;
          
          //hide sucess message after a few seconds
          setTimeout(() => {
            this.isShowingResendAccountVerificationSuccess = false;
          }, 3000);
          break;
        default:
          this.requestError1 = 'Unknown error occurred';
          this.requestError2 = '';
          return false;
      }
    } catch(err) {
      this.isLoading = false;
      
      let e = err as HttpErrorResponse;

      //log error
      // console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError1 = 'Bad request';
          this.requestError2 = '';
          break;
        case 409:
          this.requestError1 = 'Account already verified.';
          this.requestError2 = 'Please login.';
          break;
        default:
          this.requestError1 = 'Unknown error occurred';
          this.requestError2 = '';
          return false;
      }
    }

    // -- success --
    return true;
  }

  dismissModal(result) {
    this.modalController.dismiss(result);
  }

  registerNow() {
    this.dismissModal('register');
  }
}
