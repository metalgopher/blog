import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { HttpService } from '../../service/http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { emailConfMatchValidatorDirective } from './email-conf-match-validator.directive';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPageComponent implements OnInit {
  public isLoading = false;
  public requestError: string;
  public registerForm: FormGroup;
  public isUnameAvailLoading = false;
  public isUnameValidated = false;
  public isUnameAvail = false;
  public unameTakenStatusClass = '';
  private reCAPTCHA: string;

  constructor(
    private modalController: ModalController, 
    public formBuilder: FormBuilder, 
    public httpService: HttpService
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9]{3,20}$/)]],
      email: ['', [Validators.email]],
      emailConf: ['', [Validators.email]]
    }, { validators: emailConfMatchValidatorDirective });
  }

  // enables access to error controls from html
  // get errorControl() {
  //   return this.registerForm.controls;
  // }

  async sendGetUnameAvailReq(unameInput) {
    this.requestError = '';
    this.unameTakenStatusClass = '';
    this.isUnameValidated = false;
    this.isUnameAvailLoading = true;
    
    if (!this.registerForm.get('username').valid) {
      this.isUnameAvailLoading = false;
      return;
    }

    try {
      let response = await this.httpService.getUsernameAvailability(unameInput);
      
      switch (response.status) {
        case 200:
          //success
          let unameRes = response.body as any;
          this.isUnameAvail = unameRes.isAvailable;
          this.isUnameValidated = true;
          if (!this.isUnameAvail) {
            this.registerForm.get('username').setErrors({unameUnavail: true})
            this.unameTakenStatusClass = 'uname-taken';
          }
          this.isUnameAvailLoading = false;
          break;
        default:
          this.requestError = 'Unknown error occurred';
          return false;
      }
    } catch(err) {
      this.isUnameAvailLoading = false;

      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError = 'Invalid username request';
          return false;
        default:
          this.requestError = 'Unknown error occurred';
          return false;
      }
    }
  }

  async submitRegisterForm() {
    // console.log(this.registerForm.value)
    this.requestError = '';
    if (!this.registerForm.valid || !this.reCAPTCHA) {
      if (this.registerForm.errors && this.registerForm.errors.emailConfNoMatch) {
        this.requestError = 'Emails do not match';
      } else if (!this.reCAPTCHA) {
        this.requestError = 'Incomplete reCAPTCHA';
      } else if (!this.registerForm.get('username').valid) {
        this.requestError = 'Invalid username';
      } else {
        this.requestError = 'Unknown error';
      }
      this.registerForm.setErrors(null);
      return;
    }

    //handle valid form submission
    let registerResult = await this.sendRegisterVerifyEmail();
    if (!registerResult) {
      return;
    }

    //register request successful
    this.dismissModal('success');
  }

  recaptchaResolved(captchaResponse: string) {
    this.reCAPTCHA = captchaResponse;
  }

  async sendRegisterVerifyEmail() {
    try {
      let username = this.registerForm.get('username').value;
      let email = this.registerForm.get('email').value.toLowerCase();
      let emailConf = this.registerForm.get('emailConf').value.toLowerCase();
      
      this.isLoading = true;
      let response = await this.httpService.postAccountRegister(username, email, this.reCAPTCHA);

      switch (response.status) {
        case 201:
          //success
          this.requestError = '';
          break;
        default:
          this.requestError = 'Unknown error occurred';
          return false;
      }
    } catch(err) {
      this.isLoading = false;

      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 401:
          this.requestError = 'Invalid ReCAPTCHA response';
          return false;
        case 409:
          this.requestError = 'Already registered';
          return false;
        default:
          this.requestError = 'Unknown error occurred';
          return false;
      }
    }

    //success
    return true;
  }

  dismissModal(status) {
    this.modalController.dismiss(status);
  }
}
