import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';

export const emailConfMatchValidatorDirective: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const email = control.get('email').value;
  const emailConf = control.get('emailConf').value;

  return email === emailConf ? null : { emailConfNoMatch: true };
};
