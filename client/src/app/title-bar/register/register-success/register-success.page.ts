import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-register-success',
  templateUrl: './register-success.page.html',
  styleUrls: ['./register-success.page.scss'],
})
export class RegisterSuccessPageComponent implements OnInit {

  constructor(public modalController: ModalController, ) { }

  ngOnInit() {
  }

  dismissModal() {
    this.modalController.dismiss();
  }
}
