import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterSuccessPageRoutingModule } from './register-success-routing.module';

import { RegisterSuccessPageComponent } from './register-success.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterSuccessPageRoutingModule
  ],
  declarations: [RegisterSuccessPageComponent]
})
export class RegisterSuccessPageModule {}
