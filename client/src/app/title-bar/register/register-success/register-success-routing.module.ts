import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterSuccessPageComponent } from './register-success.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterSuccessPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterSuccessPageRoutingModule {}
