import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../service/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register-verify',
  templateUrl: './register-verify.page.html',
  styleUrls: ['./register-verify.page.scss'],
})
export class RegisterVerifyPageComponent implements OnInit {
  public errorMessage: string;

  constructor(
    public httpService: HttpService,
    public route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    //get token query param
    this.route.queryParamMap.subscribe((params) => {
      if (params.has('token')) {
        this.sendAccountVerifyRequest(params.get('token'));
      } else {
        this.errorMessage = 'Invalid request';
        return;
      }
    });
  }

  async sendAccountVerifyRequest(token: string) {
    try {
      let response = await this.httpService.getAccountRegisterVerify(token);
      switch (response.status) {
        case 200:
          //success
          break;
        default:
          this.errorMessage = 'An error occurred.';
          return false;
      }
    } catch(err) {
      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.errorMessage = 'Invalid request';
          return false;
        case 404:
          this.errorMessage = 'Account not found';
          return false;
        case 409:
          this.errorMessage = 'Already verified';
          return false;
        default:
          this.errorMessage = 'Unknown error occurred';
          return false;
      }
    }

    //success
    return true;
  }

  proceedToLogin() {
    this.router.navigate(['/login'], { replaceUrl: true });
  }
  
  returnHome() {
    this.router.navigate(['/home'], { replaceUrl: true });
  }
}
