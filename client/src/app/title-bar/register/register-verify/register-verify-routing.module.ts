import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterVerifyPageComponent } from './register-verify.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterVerifyPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterVerifyPageRoutingModule {}
