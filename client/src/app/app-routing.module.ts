import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: 'home',
    pathMatch: 'full',
    redirectTo: ''
  },
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'blog/home',
    pathMatch: 'full',
    redirectTo: 'blog'
  },
  {
    path: 'blog',
    loadChildren: () => import('./blog/blog-home/blog-home.module').then( m => m.BlogHomePageModule)
  },
  {
    path: 'blog/home/:page',
    loadChildren: () => import('./blog/blog-home/blog-home.module').then( m => m.BlogHomePageModule)
  },
  {
    path: 'blog/create',
    loadChildren: () => import('./articles/articles-create/articles-create.module').then( m => m.ArticlesCreatePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'blog/view/:slug',
    loadChildren: () => import('./articles/articles-view/articles-view.module').then( m => m.ArticlesViewPageModule)
  },
  {
    path: 'blog/edit/:slug',
    loadChildren: () => import('./articles/articles-edit/articles-edit.module').then( m => m.ArticlesEditPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'login',
    data: { doLogin: true },
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'register',
    pathMatch: 'full',
    data: { doRegister: true },
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'register/verify',
    pathMatch: 'full',
    loadChildren: () => import('./title-bar/register/register-verify/register-verify.module').then( m => m.RegisterVerifyPageModule)
  },
  { path: '**', redirectTo: '' },
  {
    path: 'image-full-screen-viewer',
    loadChildren: () => import('./image/image-full-screen-viewer/image-full-screen-viewer.module').then( m => m.ImageFullScreenViewerPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
