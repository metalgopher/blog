import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { HttpService } from '../http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Article } from '../../models/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleResolverService implements Resolve<Article> {

  constructor(private httpService: HttpService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Article> | Observable<never> {
    let slug = route.paramMap.get('slug');

    return from(this.getArticle(slug));
  }

  async getArticle(slug) {
    let response;
    try {
      response = await this.httpService.getArticleBySlug(slug);
      switch (response.status) {
        case 200:
          //success
          break;
        default:
          return;
      }
    } catch(err) {
      let e = err as HttpErrorResponse;

      //log error
      // console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 404:
          //handle not found
          //TODO
          this.router.navigate(['/blog'])
          return;
        default:
          return;
      }
    }

    // -- success --
    let resArticle = response.body.article as Article;
    resArticle.updatedAt = this.getFormattedArticleDate(resArticle);
    return resArticle;
  }

  private getFormattedArticleDate(article: Article): string {
    if (this.toLocaleDateStringSupportsLocales()) {
      const dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
      let updatedAtDate = new Date(article.updatedAt);
      const timeOptions = { hour: '2-digit', minute: '2-digit' };
      return `${updatedAtDate.toLocaleDateString(undefined, dateOptions)} @ ${updatedAtDate.toLocaleTimeString(undefined, timeOptions)}`;
    }

    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 
      'July', 'August', 'September', 'October', 'November', 'December'];
    let lastUpdatedAt = (new Date(article.updatedAt));
    return `${days[lastUpdatedAt.getDay()]}, ${months[lastUpdatedAt.getMonth()]} ${lastUpdatedAt.getDate()}, `
      + `${lastUpdatedAt.getFullYear()} @ ${lastUpdatedAt.getUTCHours()}:${lastUpdatedAt.getUTCMinutes()} UTC`;
  }
  
  // validation of locale support: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString
  private toLocaleDateStringSupportsLocales = function(): boolean {
    try {
      new Date().toLocaleDateString('i');
    } catch (e) {
      return e.name === 'RangeError';
    }
    return false;
  }
}
