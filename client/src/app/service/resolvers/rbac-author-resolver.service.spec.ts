import { TestBed } from '@angular/core/testing';

import { RbacAuthorResolverService } from './rbac-author-resolver.service';

describe('RbacAuthorResolverService', () => {
  let service: RbacAuthorResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RbacAuthorResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
