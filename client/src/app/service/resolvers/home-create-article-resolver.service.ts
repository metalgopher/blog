import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth.service';
import { User } from '../../models/user';
import { RbacSoftAuthorResolverService } from './rbac-soft-author-resolver.service';

@Injectable({
  providedIn: 'root'
})
export class HomeCreateArticleResolverService implements Resolve<User> {

  constructor(
    protected authorResolver: RbacSoftAuthorResolverService,
    protected authService: AuthService,
    protected router: Router
  ) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<User> {
    const identity = await this.authorResolver.resolve(route, state).toPromise();
    
    //not author
    if (!identity) {
      return;
    }

    return identity;
  }
}
