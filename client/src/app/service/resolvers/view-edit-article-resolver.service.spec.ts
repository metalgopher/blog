import { TestBed } from '@angular/core/testing';

import { ViewEditArticleResolverService } from './view-edit-article-resolver.service';

describe('ViewEditArticleResolverService', () => {
  let service: ViewEditArticleResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewEditArticleResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
