import { TestBed } from '@angular/core/testing';

import { RbacAdminResolverService } from './rbac-admin-resolver.service';

describe('RbacAdminResolverService', () => {
  let service: RbacAdminResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RbacAdminResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
