import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ArticleResolverService } from './article-resolver.service';
import { AuthService } from '../auth.service';
import { AuthRoleLevel } from '../../models/auth-role-level';
import { Article } from '../../models/article';
import { User } from '../../models/user';
import { RbacSoftAuthorResolverService } from './rbac-soft-author-resolver.service';

@Injectable({
  providedIn: 'root'
})
export class ViewEditArticleResolverService implements Resolve<{ article: Article, identity: User }> {

  constructor(
    protected articleResolver: ArticleResolverService,
    protected authorResolver: RbacSoftAuthorResolverService,
    protected authService: AuthService,
    protected router: Router
  ) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<{ article: Article, identity: User }> {
    const article = await this.articleResolver.resolve(route, state).toPromise();
    const identity = await this.authorResolver.resolve(route, state).toPromise();
    
    //if article not found, forwarded to blog home
    if (!article) {
      return;
    }

    //ensure role is admin, else that user is article author
    if (identity) {
      if (this.authService.isAuthorized(AuthRoleLevel.AdminLevel, identity.role)) {
        return { article, identity };
      } else {
        if(identity.username === article.author) {
          return { article, identity };
        }
      }
    }

    //not authorized to edit
    return { article: article, identity: null };
  }
}
