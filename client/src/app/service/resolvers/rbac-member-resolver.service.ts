import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, from } from 'rxjs';
import { RbacService } from '../rbac.service';
import { AuthRoleLevel } from '../../models/auth-role-level';
import { User } from '../../models/user';

const minRole = AuthRoleLevel.AdminLevel;

@Injectable({
  providedIn: 'root'
})
export class RbacMemberResolverService implements Resolve<User> {

  constructor(private rbacService: RbacService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Observable<never> {
    return from(this.rbacService.authorize(state, minRole));
  }
}
