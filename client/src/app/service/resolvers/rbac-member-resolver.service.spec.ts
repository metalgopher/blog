import { TestBed } from '@angular/core/testing';

import { RbacMemberResolverService } from './rbac-member-resolver.service';

describe('RbacMemberResolverService', () => {
  let service: RbacMemberResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RbacMemberResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
