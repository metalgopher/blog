import { TestBed } from '@angular/core/testing';

import { EditArticleResolverService } from './edit-article-resolver.service';

describe('EditArticleResolverService', () => {
  let service: EditArticleResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditArticleResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
