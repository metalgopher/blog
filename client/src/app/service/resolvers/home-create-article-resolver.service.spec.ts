import { TestBed } from '@angular/core/testing';

import { HomeCreateArticleResolverService } from './home-create-article-resolver.service';

describe('HomeCreateArticleResolverService', () => {
  let service: HomeCreateArticleResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomeCreateArticleResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
