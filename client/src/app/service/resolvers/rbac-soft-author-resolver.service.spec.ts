import { TestBed } from '@angular/core/testing';

import { RbacSoftAuthorResolverService } from './rbac-soft-author-resolver.service';

describe('RbacSoftAuthorResolverService', () => {
  let service: RbacSoftAuthorResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RbacSoftAuthorResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
