import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RbacAuthorResolverService } from './rbac-author-resolver.service';
import { AuthService } from '../auth.service';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class CreateArticleResolverService implements Resolve<User> {

  constructor(
    protected authorResolver: RbacAuthorResolverService,
    protected authService: AuthService,
    protected router: Router
  ) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<User> {
    const identity = await this.authorResolver.resolve(route, state).toPromise();
    
    //if not author, forward to blog home
    if (!identity) {
      this.router.navigate(['/blog']);
      return;
    }

    return identity;
  }
}
