import { TestBed } from '@angular/core/testing';

import { CreateArticleResolverService } from './create-article-resolver.service';

describe('CreateArticleResolverService', () => {
  let service: CreateArticleResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateArticleResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
