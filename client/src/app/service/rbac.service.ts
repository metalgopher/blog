import { Injectable } from '@angular/core';
import { RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { HttpService } from './http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class RbacService {

  constructor(
    private authService: AuthService,
    private httpService: HttpService,
    private router: Router
  ) { }

  async authorize(state: RouterStateSnapshot, minRole: number): Promise<User> {
    //if not logged in, prompt w/returnUrl
    if (!this.authService.currentUserValue) {
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return;
    }

    let response;
    try {
      response = await this.httpService.getAccountIdentity();
      switch (response.status) {
        case 200:
          //success
          break;
        default:
          return;
      }
    } catch(err) {
      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 401:
          this.authService.softLogout();
      }

      //prompt login with returnUrl
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return;
    }

    // -- authorize --
    let resUser = response.body as User;
    if (!this.authService.isAuthorized(minRole, resUser.role)) {
      return;
    }

    return resUser;
  }


  async softAuthorize(state: RouterStateSnapshot, minRole: number): Promise<User> {
    //if not logged in, prompt w/returnUrl
    if (!this.authService.currentUserValue) {
      return;
    }

    let response;
    try {
      response = await this.httpService.getAccountIdentity();
      switch (response.status) {
        case 200:
          //success
          break;
        default:
          return;
      }
    } catch(err) {
      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 401:
          this.authService.softLogout();
      }
      return;
    }

    // -- authorize --
    let resUser = response.body as User;
    if (!this.authService.isAuthorized(minRole, resUser.role)) {
      return;
    }

    return resUser;
  }
}
