import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { Md5 } from 'ts-md5/dist/md5';
import { Article } from '../models/article';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient,
    private authService: AuthService  
  ) { }

  async getAccountIdentity() {
    let currentUser = this.authService.currentUserValue;
    if (!currentUser) {
      throw Error('not logged in');
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      observe: 'response' as const
    };

    return this.http.get(`${environment.serviceEndpointGetIdentity}?token=${currentUser.authToken}`, httpOptions).toPromise();
  }

  async getUsernameAvailability(unameInput: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + window.btoa(`${unameInput}:${unameInput}`)
      }),
      observe: 'response' as const
    };

    return await this.http.get(environment.serviceEndpointGetUsernameAvailability, httpOptions).toPromise();
  }

  async postAccountLogin(email) {
    const md5 = new Md5();
    const emailHash = md5.appendStr(email).end();
    // console.log('emailHash: ' + emailHash);
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + window.btoa(`${emailHash}:${emailHash}`)
      }),
      observe: 'response' as const
    };

    return await this.http.post(environment.serviceEndpointPostAccountLogin, null, httpOptions).toPromise();
  }

  async postAccountLoginVerify(email, authCode) {
    const md5 = new Md5();
    const emailHash = md5.appendStr(email).end();
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + window.btoa(`${emailHash}:${authCode}`)
      }),
      observe: 'response' as const
    };

    return await this.http.post(environment.serviceEndpointPostAccountLoginVerify, null, httpOptions)
      .toPromise();
  }

  async postAccountRegister(uname, email, reCaptchaToken) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + window.btoa(`${uname}:${email}`)
      }),
      observe: 'response' as const
    };

    return await this.http.post(environment.serviceEndpointPostAccountRegister, { reCaptchaToken: reCaptchaToken }, httpOptions).toPromise();
  }

  async postAccountRegisterResendVerification(email) {
    const md5 = new Md5();
    const emailHash = md5.appendStr(email).end();

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic ' + window.btoa(`${emailHash}:${emailHash}`)
      }),
      observe: 'response' as const
    };

    return await this.http.post(environment.serviceEndpointPostAccountRegisterResendVerification, null, httpOptions).toPromise();
  }

  async getAccountRegisterVerify(token: string) {
    const httpOptions = {
      observe: 'response' as const
    };

    return await this.http.get(`${environment.serviceEndpointGetAccountRegisterVerify}?token=${token}`, httpOptions).toPromise();
  }

  async getArticleBySlug(slug: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      observe: 'response' as const
    };

    return this.http.get(environment.serviceEndpointGetArticleBySlug.replace('{slug}', slug), httpOptions).toPromise();
  }

  async postCreateArticle(article: Article) {
    let currentUser = this.authService.currentUserValue;
    if (!currentUser) {
      throw Error('not logged in');
    }

    //strip out slug/updatedAt fields
    let postArticle = new Article('', '', null, article.title, article.markdown, '', article.tags);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${currentUser.authToken}`
      }),
      observe: 'response' as const
    };

    return await this.http.post(environment.serviceEndpointPostCreateArticle, { article: postArticle }, httpOptions).toPromise();
  }

  async putEditArticle(article: Article) {
    let currentUser = this.authService.currentUserValue;
    if (!currentUser) {
      throw Error('not logged in');
    }

    //strip out slug/updatedAt fields
    let putArticle = new Article('', '', null, article.title, article.markdown, '', article.tags);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${currentUser.authToken}`
      }),
      observe: 'response' as const
    };

    return await this.http.put(environment.serviceEndpointPutEditArticle.replace('{slug}', article.slug), { article: putArticle }, httpOptions).toPromise();
  }

  async getHomeArticles(articlesPerPage: number, page: number, query: string) {
    if (page < 1) {
      throw Error('invalid page number');
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      observe: 'response' as const
    };

    return this.http.get(`${environment.serviceEndpointGetHomeArticles}?page=${page}&pageSize=${articlesPerPage}${query ? '&' + query : ''}`, httpOptions).toPromise();
  }

  async getAllArticleFilters() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
      observe: 'response' as const
    };

    return this.http.get(environment.serviceEndpointGetArticleFilters, httpOptions).toPromise();
  }
}
