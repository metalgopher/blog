import { TestBed } from '@angular/core/testing';

import { AnchorScrollerService } from './anchor-scroller.service';

describe('AnchorScrollerService', () => {
  let service: AnchorScrollerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnchorScrollerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
