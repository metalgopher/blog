import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor() {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }

  login(user: User, isReturning: boolean) {
    // store user details and jwt token in local storage to keep user logged in between page refreshes
    if (user.username && user.email && user.role && user.authToken) {
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.currentUserSubject.next(user);

      //hard refresh if not returning
      if (!isReturning) {
        window.location.href = window.location.href;
      }
    } else {
      console.log('invalid login attempted');
    }
  }

  logout() {
      // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next(null);

      //hard refresh
      window.location.href = window.location.href;
  }

  softLogout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  isAuthorized(minRole: number, role: string): boolean {
    // console.log('role: ' + role);

    if (!minRole || !role) {
      return false;
    }

    let roleMap = JSON.parse(environment.authRoleNameMap);
    if (roleMap[role] >= minRole) {
      return true;
    }

    return false;
  }
}
