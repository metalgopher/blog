import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlogHomePage } from './blog-home.page';

describe('BlogHomePage', () => {
  let component: BlogHomePage;
  let fixture: ComponentFixture<BlogHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlogHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
