import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlogHomePageRoutingModule } from './blog-home-routing.module';

import { BlogHomePage } from './blog-home.page';

import { TitleBarModule } from 'src/app/title-bar/title-bar.module';

import { ArticlePreviewerModule } from '../../articles/viewers/article-previewer/article-previewer.module';
import { FilterModalModule } from '../filter-modal/filter-modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlogHomePageRoutingModule,
    FilterModalModule,
    TitleBarModule,
    ArticlePreviewerModule
  ],
  declarations: [BlogHomePage]
})
export class BlogHomePageModule {}
