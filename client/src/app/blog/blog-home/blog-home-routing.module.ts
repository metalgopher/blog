import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogHomePage } from './blog-home.page';
import { HomeCreateArticleResolverService } from 'src/app/service/resolvers/home-create-article-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: BlogHomePage,
    resolve: {
      authorIdentity: HomeCreateArticleResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogHomePageRoutingModule {}
