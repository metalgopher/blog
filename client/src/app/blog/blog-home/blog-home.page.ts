import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadingController, NavController, IonSearchbar, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';

import { HttpService } from 'src/app/service/http.service';
import { Article } from 'src/app/models/article';
import { User } from 'src/app/models/user';
import { FilterModalComponent } from '../filter-modal/filter-modal.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-blog-home',
  templateUrl: './blog-home.page.html',
  styleUrls: ['./blog-home.page.scss'],
})
export class BlogHomePage implements OnInit, AfterViewInit, OnDestroy {
  public webPageTitle = 'Blog';
  public appPageTitle = 'Blog';
  public articles: Array<Article>;
  public currentPage = 1;
  public pages: Array<number>;
  public articlesPerPage = 5;
  public totalArticles: number;
  public requestError: string;
  public authorIdentity: User;
  public allTags: Array<string>;
  public filterTags: Array<string> = [];
  public allAuthors: Array<string>;
  public filterAuthor: string;
  public filterSearchTerms: Array<string> = [];
  private ignoredTerms = ['a', 'an', 'but', 'the', 'and', 'is', 'of'];

  @ViewChild("mdSearch") mdSearchBar: IonSearchbar;
  @ViewChild("iosSearch") iosSearchBar: IonSearchbar;

  //subscriptions
  private queryParamSub: Subscription;

  constructor(
    public platform: Platform,
    private httpService: HttpService,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private navController: NavController,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  async ngOnInit() {
    //get route role (resolver service)
    this.route.data.subscribe((data: { authorIdentity: User }) => {
      this.authorIdentity = data.authorIdentity;
    }).unsubscribe();
    // console.log('authorIdentity: ' + JSON.stringify(this.authorIdentity));

    //subscribe to params from route
    let filterTags, filterAuthor;
    this.queryParamSub = this.route.queryParamMap.subscribe(async (paramMap) => {
      //DEBUG
      // console.log('paramMap: ' + JSON.stringify(paramMap));

      //populate filter search terms
      if (paramMap.has('filterTerm')) {
        this.filterSearchTerms = [];

        //only add unique values
        let paramFilterSearchTerms = paramMap.getAll('filterTerm');
        let searchTerms = {};
        for (let i = 0; i < paramFilterSearchTerms.length; i++) {
          if (!searchTerms[paramFilterSearchTerms[i]] && !this.ignoredTerms.includes(paramFilterSearchTerms[i])) {
            this.filterSearchTerms.push(paramFilterSearchTerms[i]);
            searchTerms[paramFilterSearchTerms[i]] = true;
          }
        }
      }

      //temporarily store tags/authors from query to verify against server tags/authors
      if (paramMap.has('filterTag')) {
        filterTags = [];

        //only add unique values
        let paramFilterTags = paramMap.getAll('filterTag');
        let tags = {};
        for (let i = 0; i < paramFilterTags.length; i++) {
          if (!tags[paramFilterTags[i]]) {
            //normalize and add tag
            let tag = paramFilterTags[i].toLowerCase().split(/[^a-z0-9-"]+/).join('');
            filterTags.push(tag);
            tags[tag] = true;
          }
        }
      }

      if (paramMap.has('filterAuthor')) {
        filterAuthor = paramMap.get('filterAuthor') ? paramMap.get('filterAuthor').toLowerCase().split(/[^a-z0-9"]+/).join('') : '';
      }
  
      // console.log('this.filterAuthor: ' + this.filterAuthor);

      //verify tag/author filters and populate valid results
      if (filterTags && filterTags.length > 0 || filterAuthor) {
        //get all filters if not populated
        if (filterTags && !this.allTags || filterAuthor && !this.allAuthors) {
          await this.sendGetAllFiltersRequest();
        }
  
        if (filterTags) {
          this.filterTags = filterTags;
          for (let i = 0; i < filterTags.length; i++) {
            if (!this.allTags.includes(filterTags[i])) {
              this.removeFilterTag(i);
              return;
            }
          }
        }
        // console.log('this.filterTags: ' + this.filterTags);
    
        if (filterAuthor) {
          this.filterAuthor = filterAuthor;
          if (!this.allAuthors.includes(filterAuthor)) {
            this.removeFilterAuthor();
            return;
          }
        }
      }
      
      //update data
      await this.presentLoading('Loading...');
      await this.sendGetHomeArticlesRequest();

      //if no articles, go to first page
      if (this.currentPage > 1 && (!this.articles || this.articles.length == 0)) {
        await this.router.navigate(['/blog', 'home', 1], { queryParamsHandling: 'preserve', replaceUrl: true });
      }
      await this.loadingController.dismiss();
    });
  }

  ngAfterViewInit() {
    //get page from route params
    this.route.params.subscribe(params => {
      if (params.page) {
        this.currentPage = params.page;
        // console.log('current page: ' + this.currentPage);
      }
    }).unsubscribe();
    
    //populate searchbars
    if(this.mdSearchBar) {
      this.mdSearchBar.value = this.filterSearchTerms.join(' ');
    }
    if(this.iosSearchBar) {
      this.iosSearchBar.value = this.filterSearchTerms.join(' ');
    }
  }

  ionViewDidLeave() {
    this.articles = [];
    this.pages = null;
    this.filterAuthor = null;
    this.filterTags = [];
  }

  ngOnDestroy() {
    this.queryParamSub.unsubscribe();
  }

  getRouteQuery() {
    let queryIndex = window.location.toString().indexOf("?");
    return queryIndex >= 0 && window.location.toString().substring(queryIndex).length >= 4 ? window.location.toString().substring(queryIndex+1) : '';
  }

  async sendGetAllFiltersRequest() {
    try {
      let response = await this.httpService.getAllArticleFilters();
      switch (response.status) {
        case 200:
          //success
          this.requestError = '';
          let body = response.body as any;
          this.allTags = body.tags;
          this.allAuthors = body.authors;
          break;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    } catch(err) {
      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError = 'Invalid request';
          return;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    }
  }

  async sendGetHomeArticlesRequest() {
    try {
      this.pages = null;
      let response = await this.httpService.getHomeArticles(this.articlesPerPage, this.currentPage, this.getRouteQuery());
      switch (response.status) {
        case 200:
          //success
          this.requestError = '';
          let body = response.body as any;
          this.articles = body.articles;
          this.totalArticles = body.totalArticles;
          this.pages = [];
          for (let i = 1; i <= Math.ceil(this.totalArticles/this.articlesPerPage); i++) {
            this.pages.push(i);
          }
          break;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    } catch(err) {
      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError = 'Invalid request';
          return;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    }

    //success
  }

  async turnPage(page: number) {
    //DEBUG
    // console.log('clicked page: ' + page);

    if (this.currentPage == page) {
      return;
    }

    //nav to new page
    if(page < this.currentPage) {
      await this.navController.navigateForward(`/blog/home/${page}${this.getRouteQuery() ? '?' + this.getRouteQuery() : ''}`, {
        animated: false,
        // animationDirection: 'back'
      });
      // this.router.navigate(['/blog', 'home', page], { queryParamsHandling: 'preserve' });
    } else {
      await this.router.navigate(['/blog', 'home', page], { queryParamsHandling: 'preserve' });
    }
  }

  async presentLoading(msg: string) {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: msg
    });
    await loading.present();
  }
  
  async presentFilterModal() {
    //populate article filters if not present
    if (!this.allTags || !this.allAuthors) {
      await this.presentLoading('Loading...');
      await this.sendGetAllFiltersRequest();
      await this.loadingController.dismiss();
    }

    const modal = await this.modalController.create({
      component: FilterModalComponent,
      swipeToClose: true,
      componentProps: {
        allTags: this.allTags,
        filterTags: this.filterTags,
        allAuthors: this.allAuthors,
        filterAuthor: this.filterAuthor
      },
      cssClass: this.platform.width() >= 992 ? 'blog-filter-modal-lg' : 'blog-filter-modal'
    });

    return await modal.present();
  }

  getNormalizedSearchTerms(search: string): Array<string> {
    //get exact terms and normalize
    let exactTerms = search.trim().toLowerCase().match(/"[^"]+"/g) || [];
    // console.log('exact terms: ' + exactTerms);

    //remove exact terms from search and/or normalize
    let inexactSearch = search.trim().toLowerCase();
    for (let i = 0; i < exactTerms.length; i++) {
      let exactTermIdx = inexactSearch.indexOf(exactTerms[i]);
      if (exactTermIdx >= 0) {
        let inexactSearchTerms = inexactSearch.split('');
        inexactSearchTerms.splice(exactTermIdx, exactTerms[i].length);
        inexactSearch = inexactSearchTerms.join('');
      }

      //normalize exact term
      exactTerms[i] = exactTerms[i].split(/[^a-z0-9\s-"]+/).join('');
    }
    let normalizedTerms = exactTerms;
    normalizedTerms.push(...inexactSearch.split(/[^a-z0-9]+/).filter(term => term));

    //get rid of duplicate/ignored terms
    let uniqueTerms = [];
    let terms = {};
    for(let i = 0; i < normalizedTerms.length; i++) {
      if(!terms[normalizedTerms[i]] && !this.ignoredTerms.includes(normalizedTerms[i])) {
        uniqueTerms.push(normalizedTerms[i]);
        terms[normalizedTerms[i]] = true;
      }
    }

    return uniqueTerms;
  }

  doSearch(search: string) {
    this.filterSearchTerms = [];
    if(search) {
      this.filterSearchTerms = this.getNormalizedSearchTerms(search);
    }
  
    //psuedo nav to remove query param
    this.router.navigate(['/blog', 'home', 1], {
      queryParams: {
        filterTerm: this.filterSearchTerms
      },
      queryParamsHandling: 'merge'
    });
  }

  removeFilterTag(tagIdx: number) {
    this.filterTags.splice(tagIdx, 1);
    
    //psuedo nav to remove query param
    this.router.navigate(['/blog', 'home', 1], {
      queryParams: {
        filterTag: this.filterTags
      },
      queryParamsHandling: 'merge'
    });
  }

  removeFilterAuthor() {
    this.filterAuthor = null;

    //psuedo nav to remove query param
    this.router.navigate(['/blog', 'home', 1], {
      queryParams: {
        filterAuthor: []
      },
      queryParamsHandling: 'merge'
    });
  }

  removeFilterSearch(termIdx: number) {
    this.filterSearchTerms.splice(termIdx, 1);

    //clear search if no remaining terms
    if (this.filterSearchTerms.length === 0) {
      this.pages = null;
      if (this.iosSearchBar) {
        this.iosSearchBar.value = '';
      }
      if (this.mdSearchBar) {
        this.mdSearchBar.value = '';
      }
    }

    //psuedo nav to remove query param
    this.router.navigate(['/blog', 'home', 1], {
      queryParams: {
        filterTerm: this.filterSearchTerms
      },
      queryParamsHandling: 'merge'
    });
  }
}
