import { Component, OnInit, Input } from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'blog-filter-modal',
  templateUrl: './filter-modal.component.html',
  styleUrls: ['./filter-modal.component.scss'],
})
export class FilterModalComponent implements OnInit {
  @Input() allTags: Array<string>;
  @Input() filterTags: Array<string>;
  searchTags: Array<string>;
  @Input() allAuthors: Array<string>;
  @Input() filterAuthor: string;
  searchAuthors: Array<string>;

  constructor(
    public platform: Platform,
    private modalController: ModalController,
    private router: Router
  ) { }

  ngOnInit() {
    this.doTagSearch('');
    this.doAuthorSearch('');
  }

  doTagSearch(query: string) {
    //DEBUG
    // console.log('query: ' + query);

    this.searchTags = [];

    this.allTags.forEach(tag => {
      if (!this.filterTags.includes(tag) && (!query || tag.toLowerCase().indexOf(query.toLowerCase()) >= 0)) {
        this.searchTags.push(tag);
      } 
    });
  }

  async selectTag(tag: string, searchBar: Promise<HTMLInputElement>) {
    this.filterTags.push(tag);

    //add to query params
    this.router.navigate(['/blog', 'home', 1], {
      queryParams: {
        filterTag: this.filterTags
      },
      queryParamsHandling: 'merge'
    })

    
    let query = (await searchBar).value;
    this.doTagSearch(query);
  }

  doAuthorSearch(query: string) {
    //DEBUG
    // console.log('query: ' + query);

    this.searchAuthors = [];

    this.allAuthors.forEach(author => {
      if (this.filterAuthor !== author && (!query || author.toLowerCase().indexOf(query.toLowerCase()) >= 0)) {
        this.searchAuthors.push(author);
      }
    });
  }

  async selectAuthor(author: string, searchBar: Promise<HTMLInputElement>) {
    this.filterAuthor = author;

    //add to query params
    this.router.navigate(['/blog', 'home', 1], {
      queryParams: {
        filterAuthor: this.filterAuthor
      },
      queryParamsHandling: 'merge'
    })

    let query = (await searchBar).value;
    this.doAuthorSearch(query);
  }

  dismissModal() {
    this.modalController.dismiss();
  }
}
