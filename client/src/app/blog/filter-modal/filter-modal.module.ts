import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FilterModalComponent } from './filter-modal.component';

@NgModule({
  declarations: [FilterModalComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [FilterModalComponent]
})
export class FilterModalModule { }
