import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArticlesEditPageRoutingModule } from './articles-edit-routing.module';

import { ArticlesEditPageComponent } from './articles-edit.page';

import { TitleBarModule } from '../../title-bar/title-bar.module';

import { ArticleReaderModule } from '../viewers/article-reader/article-reader.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ArticlesEditPageRoutingModule,
    TitleBarModule,
    ArticleReaderModule
  ],
  declarations: [ArticlesEditPageComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ArticlesEditPageModule {}
