import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticlesEditPageComponent } from './articles-edit.page';
import { CanDeactivateGuard } from 'src/app/guard/can-deactivate.guard';
import { EditArticleResolverService } from 'src/app/service/resolvers/edit-article-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ArticlesEditPageComponent,
    canDeactivate: [CanDeactivateGuard],
    resolve: {
      editing: EditArticleResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticlesEditPageRoutingModule {}
