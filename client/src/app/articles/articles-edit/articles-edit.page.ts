import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Article } from 'src/app/models/article';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { HttpService } from 'src/app/service/http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-articles-edit',
  templateUrl: './articles-edit.page.html',
  styleUrls: ['./articles-edit.page.scss'],
})
export class ArticlesEditPageComponent implements OnInit {
  public pageTitle = 'Edit Article';
  public article: Article;
  public isEditMode = true;
  public updateArticleForm: FormGroup;
  public updateError: string;
  public addTagForm: FormGroup;
  public addTagError: string;
  public requestError: string;
  public isSubmitting = false;
  private editing: any;

  constructor(
    public loadingController: LoadingController,
    public formBuilder: FormBuilder, 
    public httpService: HttpService,
    private router: Router,
    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    //get editing object from route (via resolver service)
    this.route.data.subscribe((data: { editing: any }) => {
      this.editing = data.editing;
    });
    // console.log('editing: ' + JSON.stringify(this.editing, null, 2));

    //set article author
    this.article = this.editing.article;

    this.updateArticleForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      content: ['', [Validators.required, Validators.minLength(3)]]
    });

    //set form input values
    this.updateArticleForm.get('title').setValue(this.article.title);
    this.updateArticleForm.get('content').setValue(this.article.markdown);
    
    this.addTagForm = this.formBuilder.group({
      tag: ['', [Validators.minLength(3)]]
    });
  }

  async submitUpdateArticleForm() {
    // console.log(this.updateArticleForm.value);
    this.updateError = '';
    if (!this.updateArticleForm.valid) {
      // console.log(this.createArticleForm.errors);
      if (!this.updateArticleForm.get('title').valid) {
        this.updateError = 'Title too short'
      } else if (!this.updateArticleForm.get('content').valid) {
        this.updateError = 'Content too short'
      } else {
        this.updateError = 'Unknown error'
      }
      this.updateArticleForm.setErrors(null);
      return;
    }

    //debug
    // console.log(JSON.stringify(this.article, null, 2));

    //handle valid form submission
    let result = await this.sendUpdateArticle();
    
    if (result) {
      //navigate to article on success
      this.router.navigate([`/blog/view/${result}`]);
    }
    
    return true;
  }

  async submitAddTagForm() {
    this.addTagError = '';
    this.addTagForm.get('tag').setValue(this.convertTagSlug(this.addTagForm.get('tag').value));
    if (!this.addTagForm.valid) {
      if (!this.addTagForm.get('tag').valid) {
        this.addTagError = 'Tag too short.  Minimum 3 characters.'
      } else {
        this.addTagError = 'Unknown error'
      }
      this.addTagForm.setErrors(null);
      return;
    }

    //handle valid form submission
    this.addArticleTag();
  }

  private addArticleTag() {
    //change to slug form
    let tagSlug = this.convertTagSlug(this.addTagForm.get('tag').value);

    //ensure unique
    for (let i = 0; i < this.article.tags.length; i++) {
      if (this.article.tags[i] === tagSlug) {
        return false;
      }
    }

    //add tag
    this.article.tags.push(tagSlug);
    this.addTagForm.get('tag').setValue('');
    return true;
  }

  convertTagSlug(tag: string): string {
    let tagSlug = tag.trim().toLowerCase().split(/[^a-zA-Z0-9]+/).join('-');
    if (tagSlug.endsWith('-')) {
      tagSlug = tagSlug.substring(0, tagSlug.length - 1);
    }
    return tagSlug;
  }

  updateArticleTitle(title: string) {
    this.article.title = title;
  }

  updateArticleContent(content: string) {
    this.article.markdown = content;
  }

  async sendUpdateArticle() {
    await this.presentLoading('Updating article...');
    
    //response article slug
    let articleSlug: string;

    try {
      let response = await this.httpService.putEditArticle(this.article);
      this.loadingController.dismiss();
      switch (response.status) {
        case 200:
          //success
          this.requestError = '';
          let body = response.body as any;
          articleSlug = body.slug;
          break;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    } catch(err) {
      this.loadingController.dismiss();

      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError = 'Invalid request';
          return;
        case 404:
          this.requestError = 'Article not found';
          return;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    }

    //success
    return articleSlug;
  }

  submitPublish() {
    this.isSubmitting = true;
    this.changeDetector.detectChanges();
    document.getElementById('submitPubBtn').click();
    this.isSubmitting = false;
  }

  async presentLoading(msg: string) {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: msg
    });
    await loading.present();
  }
}
