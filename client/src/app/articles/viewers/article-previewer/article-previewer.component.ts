import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/models/article';
import { Router } from '@angular/router';
import { PopoverController, Platform } from '@ionic/angular';
import { AddlTagsPopoverComponent } from './addl-tags-popover/addl-tags-popover.component';

@Component({
  selector: 'app-article-previewer',
  templateUrl: './article-previewer.component.html',
  styleUrls: ['./article-previewer.component.scss'],
})
export class ArticlePreviewerComponent implements OnInit {
  @Input() article: Article;
  public maxPreviewTags = 3;

  constructor(
    public platform: Platform,
    private popoverController: PopoverController,
    private router: Router,
  ) { }

  ngOnInit() {
    this.setArticleDate();
  }

  private setArticleDate(): void {
    let lastUpdatedAt = new Date(this.article.updatedAt);
    this.article.updatedAt = `${lastUpdatedAt.getMonth()+1}/${lastUpdatedAt.getDate()}/${lastUpdatedAt.getFullYear()}`;
  }

  navChipTag(tag: string) {
    //psuedo nav to remove query param
    this.router.navigate([], { queryParams: { filterTag: tag }});
  }

  showAddlTags(ev, tags) {
    this.presentAddlTagsPopover(ev, tags);
  }

  async presentAddlTagsPopover(ev, tags) {
    const popover = await this.popoverController.create({
      component: AddlTagsPopoverComponent,
      backdropDismiss: true,
      animated: true,
      event: ev,
      componentProps: {tags: tags},
      cssClass: this.platform.width() >= 992 ? 'popover-addl-tags-lg' : 'popover-addl-tags'
    });

    return await popover.present();
  }
}
