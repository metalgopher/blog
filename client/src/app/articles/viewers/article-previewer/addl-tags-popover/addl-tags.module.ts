import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddlTagsPopoverComponent } from './addl-tags-popover.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [AddlTagsPopoverComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [AddlTagsPopoverComponent]
})
export class AddlTagsModule { }
