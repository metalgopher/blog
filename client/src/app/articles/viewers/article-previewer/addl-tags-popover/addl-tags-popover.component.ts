import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-addl-tags-popover',
  templateUrl: './addl-tags-popover.component.html',
  styleUrls: ['./addl-tags-popover.component.scss'],
})
export class AddlTagsPopoverComponent implements OnInit {
  @Input() tags: Array<string>;

  constructor(
    private router: Router,
    private popoverController: PopoverController
  ) { }

  ngOnInit() {}

  navChipTag(tag: string) {
    this.popoverController.dismiss();

    this.router.navigate(['/blog'], { queryParams: { filterTag: tag }});
  }
}
