import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ArticlePreviewerComponent } from './article-previewer.component';

import { PipesModule } from 'src/app/pipes/pipes-module.module';

import { RouterModule } from '@angular/router';
import { AddlTagsModule } from './addl-tags-popover/addl-tags.module';

@NgModule({
  declarations: [ArticlePreviewerComponent],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule,
    RouterModule,
    AddlTagsModule
  ],
  exports: [ArticlePreviewerComponent]
})
export class ArticlePreviewerModule { }
