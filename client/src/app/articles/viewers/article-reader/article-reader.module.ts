import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { ArticleReaderComponent } from './article-reader.component';

import { PipesModule } from 'src/app/pipes/pipes-module.module';

@NgModule({
  declarations: [ArticleReaderComponent],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule
  ],
  exports: [ArticleReaderComponent]
})
export class ArticleReaderModule { }
