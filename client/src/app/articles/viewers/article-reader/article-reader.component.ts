import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/models/article';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article-reader',
  templateUrl: './article-reader.component.html',
  styleUrls: ['./article-reader.component.scss'],
})
export class ArticleReaderComponent implements OnInit {
  @Input() article: Article;
  @Input() isEditMode: boolean;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  removeTag(tagIdx: number) {
    this.article.tags.splice(tagIdx, 1);
  }

  navChipTag(tag: string) {
    if(this.isEditMode) {
      return;
    }
    this.router.navigate(['/blog'], { queryParams: { filterTag: tag } });
  }
}
