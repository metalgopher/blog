import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArticlesViewPageRoutingModule } from './articles-view-routing.module';

import { ArticlesViewPageComponent } from './articles-view.page';

import { TitleBarModule } from '../../title-bar/title-bar.module';

import { ArticleReaderModule } from '../viewers/article-reader/article-reader.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArticlesViewPageRoutingModule,
    TitleBarModule,
    ArticleReaderModule
  ],
  declarations: [ArticlesViewPageComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ArticlesViewPageModule {}
