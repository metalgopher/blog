import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticlesViewPageComponent } from './articles-view.page';
import { CanDeactivateGuard } from 'src/app/guard/can-deactivate.guard';
import { ViewEditArticleResolverService } from 'src/app/service/resolvers/view-edit-article-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ArticlesViewPageComponent,
    canDeactivate: [CanDeactivateGuard],
    resolve: {
      viewEditArticle: ViewEditArticleResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticlesViewPageRoutingModule {}
