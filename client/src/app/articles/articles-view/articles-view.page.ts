import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/models/article';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-articles-view',
  templateUrl: './articles-view.page.html',
  styleUrls: ['./articles-view.page.scss'],
})
export class ArticlesViewPageComponent implements OnInit {
  public webPageTitle = 'Blog';
  public appPageTitle = 'Blog';
  public article: Article;
  public showEditOption: boolean;
  public isEditMode = false;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    //get article from route (via resolver service)
    this.route.data.subscribe((data: { viewEditArticle: { article: Article, identity: User } }) => {
      this.article = data.viewEditArticle.article;
      this.appPageTitle += ` - ${this.article.title}`;
      this.showEditOption = !!data.viewEditArticle.identity;
    });
  }
}
