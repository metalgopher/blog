import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArticlesCreatePageRoutingModule } from './articles-create-routing.module';

import { ArticlesCreatePageComponent } from './articles-create.page';

import { TitleBarModule } from '../../title-bar/title-bar.module';

import { ArticleReaderModule } from '../viewers/article-reader/article-reader.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ArticlesCreatePageRoutingModule,
    TitleBarModule,
    ArticleReaderModule
  ],
  declarations: [ArticlesCreatePageComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ArticlesCreatePageModule {}
