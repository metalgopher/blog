import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Article } from 'src/app/models/article';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-articles-create',
  templateUrl: './articles-create.page.html',
  styleUrls: ['./articles-create.page.scss'],
})
export class ArticlesCreatePageComponent implements OnInit {
  public pageTitle = 'Create New Article';
  public article: Article = new Article('', '', (new Date()).toISOString(), '', '', '', []);
  public isEditMode = true;
  public createArticleForm: FormGroup;
  public createError: string;
  public addTagForm: FormGroup;
  public addTagError: string;
  public requestError: string;
  public isSubmitting = false;
  private identity: User;

  constructor(
    public loadingController: LoadingController,
    public formBuilder: FormBuilder,
    public httpService: HttpService,
    public authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    //get route role (resolver service)
    this.route.data.subscribe((data: { identity: User }) => {
      this.identity = data.identity;
    });
    // console.log('creator identity: ' + JSON.stringify(this.identity));

    //set article author
    this.article.author = this.identity.username;

    this.createArticleForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      content: ['', [Validators.required, Validators.minLength(3)]]
    });
    
    this.addTagForm = this.formBuilder.group({
      tag: ['', [Validators.minLength(3)]]
    });
  }

  async submitCreateArticleForm() {
    // console.log(this.createArticleForm.value);
    this.createError = '';
    if (!this.createArticleForm.valid) {
      // console.log(this.createArticleForm.errors);
      if (!this.createArticleForm.get('title').valid) {
        this.createError = 'Title too short'
      } else if (!this.createArticleForm.get('content').valid) {
        this.createError = 'Content too short'
      } else {
        this.createError = 'Unknown error'
      }
      this.createArticleForm.setErrors(null);
      return;
    }

    //debug
    // console.log(JSON.stringify(this.article, null, 2));

    //handle valid form submission
    let result = await this.sendCreateArticle();
    
    if (result) {
      //navigate to article on success
      this.router.navigate([`/blog/view/${result}`]);
    }
    
    return true;
  }

  async submitAddTagForm() {
    this.addTagError = '';
    this.addTagForm.get('tag').setValue(this.convertTagSlug(this.addTagForm.get('tag').value));
    if (!this.addTagForm.valid) {
      if (!this.addTagForm.get('tag').valid) {
        this.addTagError = 'Tag too short.  Minimum 3 characters.'
      } else {
        this.addTagError = 'Unknown error'
      }
      this.addTagForm.setErrors(null);
      return;
    }

    //handle valid form submission
    this.addArticleTag();
  }

  private addArticleTag() {
    //change to slug form
    let tagSlug = this.convertTagSlug(this.addTagForm.get('tag').value);

    //ensure unique
    for (let i = 0; i < this.article.tags.length; i++) {
      if (this.article.tags[i] === tagSlug) {
        return false;
      }
    }

    //add tag
    this.article.tags.push(tagSlug);
    this.addTagForm.get('tag').setValue('');
    return true;
  }

  convertTagSlug(tag: string): string {
    let tagSlug = tag.trim().toLowerCase().split(/[^a-zA-Z0-9]+/).join('-');
    if (tagSlug.endsWith('-')) {
      tagSlug = tagSlug.substring(0, tagSlug.length - 1);
    }
    return tagSlug;
  }

  updateArticleTitle(title: string) {
    this.article.title = title;
  }

  updateArticleContent(content: string) {
    this.article.markdown = content;
  }

  async sendCreateArticle() {
    await this.presentLoading('Creating article...');
    
    //response article slug
    let articleSlug: string;

    try {
      let response = await this.httpService.postCreateArticle(this.article);
      this.loadingController.dismiss();
      switch (response.status) {
        case 201:
          //success
          this.requestError = '';
          let body = response.body as any;
          articleSlug = body.slug;
          break;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    } catch(err) {
      this.loadingController.dismiss();

      let e = err as HttpErrorResponse;

      //log error
      console.log(e.message);

      //set error ui message
      switch (e.status) {
        case 400:
          this.requestError = 'Invalid request';
          return;
        default:
          this.requestError = 'Unknown error occurred';
          return;
      }
    }

    //success
    return articleSlug;
  }

  submitPublish() {
    this.isSubmitting = true;
    this.changeDetector.detectChanges();
    document.getElementById('submitPubBtn').click();
    this.isSubmitting = false;
  }

  async presentLoading(msg: string) {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      message: msg
    });
    await loading.present();
  }
}
