import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticlesCreatePageComponent } from './articles-create.page';
import { CanDeactivateGuard } from 'src/app/guard/can-deactivate.guard';
import { CreateArticleResolverService } from 'src/app/service/resolvers/create-article-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ArticlesCreatePageComponent,
    canDeactivate: [CanDeactivateGuard],
    resolve: {
      identity: CreateArticleResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticlesCreatePageRoutingModule {}
