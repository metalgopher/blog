import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { User } from './models/user';
import { environment } from 'src/environments/environment';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public menuOptions = [
    {
      title: 'Home',
      url: '/',
      altUrls: ['/home'],
      icon: 'home'
    },
    {
      title: 'About',
      url: '/about',
      icon: 'list'
    },
    {
      title: 'Blog',
      url: '/blog',
      icon: 'reader'
    },
    {
      title: 'Code',
      extUrl: environment.codeProjectURL,
      icon: 'code'
    },
    {
      title: 'FAQ',
      url: '/faq',
      icon: 'help-circle'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router

  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname;
    if (path) {
      this.selectedIndex = this.getSelectedIndexByPath(path) > -1 ? this.getSelectedIndexByPath(path) : this.getSelectedIndexByPath('/home');
    }

    this.router.events.subscribe((val) => {
        // console.log('val: ' + val);
        if (val instanceof NavigationEnd) {
          let nav = val as NavigationEnd;
          this.selectedIndex = this.getSelectedIndexByPath(nav.urlAfterRedirects);
        }
    });
  }

  getSelectedIndexByPath(path: string): number {
    return this.menuOptions.findIndex(option => {
      if (!option.url) { return false }
      if (path.toLowerCase() === '/') { return option.url === '/' }

      return option.url.length > 1 && path.toLowerCase().startsWith(option.url.toLowerCase())
        || option.altUrls?.filter(altUrl => path.toLowerCase().startsWith(altUrl.toLowerCase())).length > 0
    });
  }
}
