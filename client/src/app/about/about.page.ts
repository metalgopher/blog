import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ImageFullScreenViewerPageComponent } from '../image/image-full-screen-viewer/image-full-screen-viewer.page';
import { AnchorScrollerService } from '../service/anchor-scroller.service';
import { AuthService } from '../service/auth.service';
import { User } from '../models/user';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  public pageTitle = 'About';
  public currentYear: number;
  public currentUser: User;

  constructor(
    private modalController: ModalController,
    private anchorScrollerService: AnchorScrollerService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.currentYear = (new Date()).getFullYear();

    this.anchorScrollerService.listen();

    this.authService.currentUser.subscribe(user => this.currentUser = user);
  }

  async presentImageFullScreen(imgSrc: string) {
    const modal = await this.modalController.create({
      component: ImageFullScreenViewerPageComponent,
      swipeToClose: true,
      componentProps: { imgSrc: imgSrc },
      cssClass: 'full-screen'
    });

    return await modal.present();
  }

  showSystemDesignFullScreen() {
    this.presentImageFullScreen("../../assets/img/systemDesign.svg");
  }
}
