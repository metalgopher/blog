import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePageComponent implements OnInit {
  public pageTitle = 'Home';
  public logoFullSrc = '../../assets/img/logoFull.png';
  public currentYear: number;

  constructor() { }
  
  ngOnInit() {
    this.currentYear = (new Date()).getFullYear();
  }
}
