export class User {
    username: string;
    email: string;
    role: string;
    authToken: string;
    
    constructor(username: string, authToken: string) {
        this.username = username;
        this.authToken = authToken;
    }
}
