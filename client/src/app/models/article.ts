export class Article {
    public slug: string;
    public author: string;
    public updatedAt: string;
    public title: string;
    public markdown: string;
    public previewText: string;
    public tags: Array<string>;
    
    constructor(slug: string, author: string, updatedAt: string, title: string, markdown: string, previewText: string, tags: Array<string>) {
        this.slug = slug;
        this.author = author;
        this.updatedAt = updatedAt;
        this.title = title;
        this.markdown = markdown;
        this.previewText = previewText;
        this.tags = tags;
    }
}
