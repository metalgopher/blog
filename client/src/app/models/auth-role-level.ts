export class AuthRoleLevel {
    public static MemberLevel = 1;
    public static AuthorLevel = 2;
    public static AdminLevel = 3;
}
