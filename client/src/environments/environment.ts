// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  codeProjectURL: 'https://gitlab.com/metalgopher/cloudbark',
  cryptoSalt: '$2a$11$Cxm4JBGbcO2YIVJOXR0GG.',
  authRoleNameMap: '{ "mem": 1, "auth": 2, "adm": 3 }',
  serviceEndpointGetIdentity: 'http://localhost:8080/api/account/identity',
  serviceEndpointGetUsernameAvailability: 'http://localhost:8080/api/account/register/uname/availability',
  serviceEndpointPostAccountLogin: 'http://localhost:8080/api/account/login',
  serviceEndpointPostAccountLoginVerify: 'http://localhost:8080/api/account/login/verify',
  serviceEndpointPostAccountRegister: 'http://localhost:8080/api/account/register',
  serviceEndpointGetAccountRegisterVerify: 'http://localhost:8080/api/account/register/verify',
  serviceEndpointPostAccountRegisterResendVerification: 'http://localhost:8080/api/account/register/verify/resend',
  serviceEndpointGetArticleBySlug: 'http://localhost:8080/api/article/view/{slug}',
  serviceEndpointPostCreateArticle: 'http://localhost:8080/api/article',
  serviceEndpointPutEditArticle: 'http://localhost:8080/api/article/edit/{slug}',
  serviceEndpointGetHomeArticles: 'http://localhost:8080/api/article/home',
  serviceEndpointGetArticleFilters: 'http://localhost:8080/api/article/filters',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
