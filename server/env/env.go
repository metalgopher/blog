package env

import (
	"encoding/json"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go/v4"
	"github.com/sirupsen/logrus"
)

//Env represents the current configured environment of the server
type Env int

const (
	//EnvDev indicates the server is configured for the dev environment
	EnvDev Env = iota + 1

	//EnvStage indicates the server is configured for the stage environment
	EnvStage

	//EnvProd indicates the server is configured for the prod environment
	EnvProd
)

var env *Config

//Config is the app config
type Config struct {
	AppHostProtocol                 string
	AppHostName                     string
	AppName                         string
	AppCompany                      string
	AppCompanyLink                  string
	ServerTZ                        *time.Location
	ServerEnv                       Env
	ServerPort                      string
	ServerLogLevel                  logrus.Level
	AuthLoginEmailFrom              string
	AuthLoginCodeDigits             int
	AuthLoginCodeSpaceIndexes       map[int]interface{}
	AuthLoginCodeTTLSeconds         int
	AuthJWTSigningAlgo              jwt.SigningMethod
	AuthJWTSigningKey               []byte
	AuthJWTLoginExpSeconds          float64
	AuthJWTAcctVerifyExpSeconds     float64
	AuthJWTIssuer                   string
	AuthRoleNameMap                 map[string]string
	DBConnectionHost                string
	DBConnectionPort                string
	DBConnectionUname               string
	DBConnectionPword               string
	DBSchema                        string
	DBScripts                       string
	ProviderEmailName               string
	ProviderEmailAuthHeader         string
	ProviderEmailEndpointSendTX     string
	ProviderRecaptchaSecretKey      string
	ProviderRecaptchaEndpointVerify string
}

//Configure sets up the global configuration for the server
func Configure() {
	env = &Config{}

	//APP_HOST_PROTOCOL
	if val, isPresent := os.LookupEnv("APP_HOST_PROTOCOL"); isPresent {
		env.AppHostProtocol = val
	}

	//APP_HOST_NAME
	if val, isPresent := os.LookupEnv("APP_HOST_NAME"); isPresent {
		env.AppHostName = val
	}

	//APP_NAME
	if val, isPresent := os.LookupEnv("APP_NAME"); isPresent {
		env.AppName = val
	}

	//APP_COMPANY
	if val, isPresent := os.LookupEnv("APP_COMPANY"); isPresent {
		env.AppCompany = val
	}

	//APP_COMPANY_LINK
	if val, isPresent := os.LookupEnv("APP_COMPANY_LINK"); isPresent {
		env.AppCompanyLink = val
	}

	//ServerTZ
	if loc, err := time.LoadLocation("Local"); err == nil {
		env.ServerTZ = loc
	} else {
		panic(err)
	}

	//SERVER_ENV
	if val, isPresent := os.LookupEnv("SERVER_ENV"); isPresent {
		switch strings.ToLower(val) {
		case "dev":
			env.ServerEnv = EnvDev
		case "stage":
			env.ServerEnv = EnvStage
		case "prod":
			env.ServerEnv = EnvProd
		default:
			panic("invalid SERVER_ENV detected: " + val)
		}
	} else {
		panic("missing SERVER_ENV")
	}

	//SERVER_PORT
	if val, isPresent := os.LookupEnv("SERVER_PORT"); isPresent {
		env.ServerPort = val
	} else {
		env.ServerPort = "8080"
	}

	//SERVER_LOG_LEVEL
	if val, isPresent := os.LookupEnv("SERVER_LOG_LEVEL"); isPresent {
		switch strings.ToLower(val) {
		case "trace":
			env.ServerLogLevel = logrus.TraceLevel
		case "debug":
			env.ServerLogLevel = logrus.DebugLevel
		case "info":
			env.ServerLogLevel = logrus.InfoLevel
		case "warn":
			env.ServerLogLevel = logrus.WarnLevel
		case "error":
			env.ServerLogLevel = logrus.ErrorLevel
		case "fatal":
			env.ServerLogLevel = logrus.FatalLevel
		case "panic":
			env.ServerLogLevel = logrus.PanicLevel
		default:
			panic("invalid SERVER_LOG_LEVEL detected: " + val)
		}
	} else {
		env.ServerLogLevel = logrus.InfoLevel
	}

	//AUTH_LOGIN_EMAIL_FROM
	if val, isPresent := os.LookupEnv("AUTH_LOGIN_EMAIL_FROM"); isPresent {
		env.AuthLoginEmailFrom = val
	}

	//AUTH_LOGIN_CODE_DIGITS
	if val, isPresent := os.LookupEnv("AUTH_LOGIN_CODE_DIGITS"); isPresent {
		var err error
		if env.AuthLoginCodeDigits, err = strconv.Atoi(val); err != nil {
			panic(err)
		}
	}

	//AUTH_LOGIN_CODE_DIGITS
	if val, isPresent := os.LookupEnv("AUTH_LOGIN_CODE_DIGITS"); isPresent {
		var err error
		if env.AuthLoginCodeDigits, err = strconv.Atoi(val); err != nil {
			panic(err)
		}
	}

	//AUTH_LOGIN_CODE_SPACE_INDEXES
	if val, isPresent := os.LookupEnv("AUTH_LOGIN_CODE_SPACE_INDEXES"); isPresent {
		env.AuthLoginCodeSpaceIndexes = make(map[int]interface{})

		for _, idx := range strings.Split(val, ",") {
			if idxVal, err := strconv.Atoi(idx); err == nil {
				env.AuthLoginCodeSpaceIndexes[idxVal] = nil
			} else {
				panic(err)
			}
		}
	}

	//AUTH_LOGIN_CODE_TTL_SECONDS
	if val, isPresent := os.LookupEnv("AUTH_LOGIN_CODE_TTL_SECONDS"); isPresent {
		var err error
		if env.AuthLoginCodeTTLSeconds, err = strconv.Atoi(val); err != nil {
			panic(err)
		}
	}

	//AUTH_JWT_SIGNING_ALGO
	if val, _ := os.LookupEnv("AUTH_JWT_SIGNING_ALGO"); true {
		switch val {
		case "HS256":
			env.AuthJWTSigningAlgo = jwt.SigningMethodHS256
		default:
			env.AuthJWTSigningAlgo = jwt.SigningMethodHS256
		}
	}

	//AUTH_JWT_SIGNING_KEY
	if val, isPresent := os.LookupEnv("AUTH_JWT_SIGNING_KEY"); isPresent {
		env.AuthJWTSigningKey = []byte(val)
	}

	//AUTH_JWT_LOGIN_EXP_SECONDS
	if val, isPresent := os.LookupEnv("AUTH_JWT_LOGIN_EXP_SECONDS"); isPresent {
		if seconds, err := strconv.ParseFloat(val, 64); err == nil {
			env.AuthJWTLoginExpSeconds = seconds
		} else {
			panic(err)
		}
	}

	//AUTH_JWT_ACCT_VERIFY_EXP_SECONDS
	if val, isPresent := os.LookupEnv("AUTH_JWT_ACCT_VERIFY_EXP_SECONDS"); isPresent {
		if seconds, err := strconv.ParseFloat(val, 64); err == nil {
			env.AuthJWTAcctVerifyExpSeconds = seconds
		} else {
			panic(err)
		}
	}

	//AUTH_JWT_ISSUER
	if val, isPresent := os.LookupEnv("AUTH_JWT_ISSUER"); isPresent {
		env.AuthJWTIssuer = val
	}

	//AUTH_ROLE_CLIENT_NAME_MAP
	if val, isPresent := os.LookupEnv("AUTH_ROLE_CLIENT_NAME_MAP"); isPresent {
		//parse to map from JSON
		if err := json.Unmarshal([]byte(val), &env.AuthRoleNameMap); err != nil {
			panic(err)
		}
	}

	//DB_CONNECTION_UNAME
	if val, isPresent := os.LookupEnv("DB_CONNECTION_UNAME"); isPresent {
		env.DBConnectionUname = val
	} else {
		panic("missing DB_CONNECTION_UNAME")
	}

	//DB_CONNECTION_PWORD
	if val, isPresent := os.LookupEnv("DB_CONNECTION_PWORD"); isPresent {
		env.DBConnectionPword = val
	} else {
		panic("missing DB_CONNECTION_PWORD")
	}

	//DB_CONNECTION_HOST
	if val, isPresent := os.LookupEnv("DB_CONNECTION_HOST"); isPresent {
		env.DBConnectionHost = val
	} else {
		panic("missing DB_CONNECTION_HOST")
	}

	//DB_CONNECTION_PORT
	if val, isPresent := os.LookupEnv("DB_CONNECTION_PORT"); isPresent {
		env.DBConnectionPort = val
	} else {
		panic("missing DB_CONNECTION_PORT")
	}

	//DB_SCHEMA
	if val, isPresent := os.LookupEnv("DB_SCHEMA"); isPresent {
		env.DBSchema = val
	} else {
		panic("missing DB_SCHEMA")
	}

	//DB_SCRIPTS
	if val, isPresent := os.LookupEnv("DB_SCRIPTS"); isPresent {
		env.DBScripts = val
	}

	//PROVIDER_EMAIL_NAME
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_NAME"); isPresent {
		env.ProviderEmailName = val
	}

	//PROVIDER_EMAIL_AUTH_HEADER
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_AUTH_HEADER"); isPresent {
		env.ProviderEmailAuthHeader = val
	}

	//PROVIDER_EMAIL_ENDPOINT_SEND_TX
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_ENDPOINT_SEND_TX"); isPresent {
		env.ProviderEmailEndpointSendTX = val
	}

	//PROVIDER_RECAPTCHA_SECRET_KEY
	if val, isPresent := os.LookupEnv("PROVIDER_RECAPTCHA_SECRET_KEY"); isPresent {
		env.ProviderRecaptchaSecretKey = val
	}

	//PROVIDER_RECAPTCHA_ENDPOINT_VERIFY
	if val, isPresent := os.LookupEnv("PROVIDER_RECAPTCHA_ENDPOINT_VERIFY"); isPresent {
		env.ProviderRecaptchaEndpointVerify = val
	}
}

//GetConfig returns the unexported environment to outside packages
func GetConfig() *Config {
	//return read-only copy
	envCopy := *env

	//copy pointer/reference values
	tz := *env.ServerTZ
	envCopy.ServerTZ = &tz

	alcsi := make(map[int]interface{})
	for idx := range envCopy.AuthLoginCodeSpaceIndexes {
		alcsi[idx] = nil
	}
	envCopy.AuthLoginCodeSpaceIndexes = alcsi

	signingKey := string(envCopy.AuthJWTSigningKey)
	envCopy.AuthJWTSigningKey = []byte(signingKey)

	return &envCopy
}
