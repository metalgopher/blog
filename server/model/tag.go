package model

//Tag represents an article tag
type Tag struct {
	BaseNoID
	DisplayName string `json:"displayName"`
}
