package model

import (
	"strings"

	"github.com/PuerkitoBio/goquery"
)

//Article represents an article model
type Article struct {
	Base
	URLIdentifier string         `json:"slug"`
	AuthorUname   string         `json:"author"`
	Title         string         `json:"title"`
	Markdown      string         `json:"markdown"`
	TextContent   string         `json:"previewText"`
	Tags          map[string]Tag `json:"-"`
	TagNames      []string       `json:"tags"`
}

//ToView handler that transforms for client/view
func (a *Article) ToView() {
	if a.TagNames == nil {
		a.TagNames = make([]string, 0)
	}
	if a.Tags != nil {
		for tagName := range a.Tags {
			a.TagNames = append(a.TagNames, tagName)
		}
	}

	//reduce article text content for preview
	if len([]rune(a.TextContent)) > 500 {
		a.TextContent = string([]rune(a.TextContent)[:500])
	}
}

//FromView handler that transforms from client/view
func (a *Article) FromView() {
	if a.TagNames != nil {
		if a.Tags == nil {
			a.Tags = make(map[string]Tag)
		}
		for _, tagName := range a.TagNames {
			a.Tags[tagName] = Tag{DisplayName: tagName}
		}

		//pull out text content from markdown via goquery
		doc, _ := goquery.NewDocumentFromReader(strings.NewReader(a.Markdown))
		a.TextContent = doc.Text()
	}
}
