package model

import (
	"strconv"
	"time"

	"server/env"
)

//Account represents a user account
type Account struct {
	Base
	Email       string
	EmailHash   string
	Username    string
	Role        AcctRole
	AuthCode    *string
	AuthCodeExp *time.Time
	JWT         *string
	VerifiedAt  *time.Time
}

const (
	//AcctRoleMember is a normal Member role
	AcctRoleMember = iota + 1

	//AcctRoleAuthor is an author role (create and edit own)
	AcctRoleAuthor

	//AcctRoleAdmin is an admin role (all permissions)
	AcctRoleAdmin
)

//AcctRole is an enum for Account auth roles
type AcctRole int

//Valid returns whether role is valid
func (ar AcctRole) Valid() bool {
	return ar >= AcctRoleMember && ar <= AcctRoleAdmin
}

//Name returns string name of role
func (ar AcctRole) Name() string {
	switch ar {
	case AcctRoleMember:
		return "MEMBER"
	case AcctRoleAuthor:
		return "AUTHOR"
	case AcctRoleAdmin:
		return "ADMIN"
	}
	return ""
}

//ClientName returns string name of role for the client
func (ar AcctRole) ClientName() string {
	switch ar {
	case AcctRoleMember:
		return env.GetConfig().AuthRoleNameMap[strconv.Itoa(AcctRoleMember)]
	case AcctRoleAuthor:
		return env.GetConfig().AuthRoleNameMap[strconv.Itoa(AcctRoleAuthor)]
	case AcctRoleAdmin:
		return env.GetConfig().AuthRoleNameMap[strconv.Itoa(AcctRoleAdmin)]
	}
	return ""
}

//GetAllAcctRoles returns a map of all AcctRole values to their names
func GetAllAcctRoles() map[AcctRole]string {
	return map[AcctRole]string{
		AcctRoleMember: AcctRole.Name(AcctRoleMember),
		AcctRoleAuthor: AcctRole.Name(AcctRoleAuthor),
		AcctRoleAdmin:  AcctRole.Name(AcctRoleAdmin),
	}
}
