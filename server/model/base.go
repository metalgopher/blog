package model

import (
	"time"
)

//Base is the base data model
type Base struct {
	ID        int        `json:"-"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"updatedAt,omitempty"`
	DeletedAt *time.Time `json:"-"`
}

//BaseNoID is the base data model without an ID
type BaseNoID struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
