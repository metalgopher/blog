module server

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/websocket v1.4.1
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
)
