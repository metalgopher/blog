package auth

import (
	"fmt"
	"math/rand"
	"server/model"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go/v4"
)

//GenerateNumericCodeString generates a random auth code in string form, adding spaces at given indexes if any
func GenerateNumericCodeString(digits int, spaceIndexes map[int]interface{}, seed int64) (string, error) {
	if digits <= 0 {
		return "", fmt.Errorf("digits must be positive")
	}

	rand.Seed(seed)

	code := strings.Builder{}

	for i := 0; i < digits; i++ {
		if spaceIndexes != nil {
			if _, ok := spaceIndexes[i]; ok {
				code.WriteRune(' ')
			}
		}
		code.WriteString(strconv.Itoa(rand.Intn(10)))
	}

	return code.String(), nil
}

//UserClaimsJWT is the model for user JWT claims
type UserClaimsJWT struct {
	jwt.StandardClaims
	Username string         `json:"username"`
	Email    string         `json:"email"`
	Role     model.AcctRole `json:"role"`
}
