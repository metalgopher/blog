package auth_test

import (
	"fmt"
	"testing"

	"server/api/http/util/auth"
)

type numericCodeTestParams struct {
	digits       int
	spaceIndexes map[int]interface{}
	seed         int64
}

type numericCodeTestData struct {
	data         numericCodeTestParams
	expectedCode string
	expectedErr  error
}

func numericCodeTests() []numericCodeTestData {
	return []numericCodeTestData{
		{
			data:         numericCodeTestParams{digits: 6, spaceIndexes: map[int]interface{}{3: nil}, seed: 8},
			expectedCode: "889 666",
			expectedErr:  nil,
		},
		{
			data:         numericCodeTestParams{digits: 6, spaceIndexes: map[int]interface{}{3: nil}, seed: 88},
			expectedCode: "701 626",
			expectedErr:  nil,
		},
		{
			data:         numericCodeTestParams{digits: 6, spaceIndexes: map[int]interface{}{3: nil}, seed: 888},
			expectedCode: "439 142",
			expectedErr:  nil,
		},
		{
			data:         numericCodeTestParams{digits: 8, spaceIndexes: map[int]interface{}{4: nil}, seed: 333},
			expectedCode: "9960 8567",
			expectedErr:  nil,
		},
		{
			data:         numericCodeTestParams{digits: -10, spaceIndexes: map[int]interface{}{4: nil, 8: nil}, seed: 555},
			expectedCode: "",
			expectedErr:  fmt.Errorf("digits must be positive"),
		},
		{
			data:         numericCodeTestParams{digits: -1, spaceIndexes: map[int]interface{}{4: nil, 8: nil}, seed: 555},
			expectedCode: "",
			expectedErr:  fmt.Errorf("digits must be positive"),
		},
		{
			data:         numericCodeTestParams{digits: 0, spaceIndexes: map[int]interface{}{4: nil, 8: nil}, seed: 555},
			expectedCode: "",
			expectedErr:  fmt.Errorf("digits must be positive"),
		},
	}
}

func TestGenerateNumericCodeString(t *testing.T) {
	for idx, test := range numericCodeTests() {
		actual, err := auth.GenerateNumericCodeString(test.data.digits, test.data.spaceIndexes, test.data.seed)
		if actual != test.expectedCode {
			t.Errorf("TestGenerateNumericCodeString(%d) >> actual code: %s, expected code: %s", idx, actual, test.expectedCode)
		}
		if err == nil && test.expectedErr != nil || err != nil && test.expectedErr == nil || err != nil && test.expectedErr != nil && err.Error() != test.expectedErr.Error() {
			t.Errorf("TestGenerateNumericCodeString(%d) >> actual error: %s, expected error: %s", idx, err, test.expectedErr)
		}
	}
}
