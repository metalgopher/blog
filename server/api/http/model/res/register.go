package res

//UsernameAvailability response model
type UsernameAvailability struct {
	IsAvailable bool `json:"isAvailable"`
}
