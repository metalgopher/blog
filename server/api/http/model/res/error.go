package res

const (
	//ErrorCodeInvalidCreds is the error code for when credentials are not valid
	ErrorCodeInvalidCreds ErrorCode = iota + 1

	//ErrorCodeExpiredCreds is the error code for when credentials are valid but expired
	ErrorCodeExpiredCreds
)

//ErrorCode is a code returned to HTTP clients in case of error
type ErrorCode int

//Valid returns whether enum value is valid (within range)
func (ec ErrorCode) Valid() bool {
	if ec < ErrorCodeInvalidCreds || ec > ErrorCodeExpiredCreds {
		return false
	}

	return true
}

//GetSimpleErr returns a SimpleError with this code and a default message
func (ec ErrorCode) GetSimpleErr() SimpleError {
	err := SimpleError{}
	switch ec {
	case ErrorCodeInvalidCreds:
		err.Err = "invalid credentials"
	case ErrorCodeExpiredCreds:
		err.Err = "credentials expired"
	}
	return err
}

//SimpleError is the model for a simple error response with just a message
type SimpleError struct {
	Err     string    `json:"err"`
	ErrCode ErrorCode `json:"errCode,omitempty"`
}
