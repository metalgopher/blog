package res

//SimpleMsg is a simple response with a msg field
type SimpleMsg struct {
	Msg string `json:"msg"`
}

//RecaptchaVerify is the response model for the reCAPTCHA verify endpoint
type RecaptchaVerify struct {
	Success    bool     `json:"success"`
	ErrorCodes []string `json:"error-codes"`
}

//Login is the response for a successful login
type Login struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Role     string `json:"role"`
	JWT      string `json:"authToken"`
}

//GetIdentify is the response for a get identity request
type GetIdentify struct {
	Username string `json:"username"`
	Role     string `json:"role"`
}
