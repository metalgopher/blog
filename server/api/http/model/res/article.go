package res

import (
	"server/model"
)

//CreateArticle response model
type CreateArticle struct {
	URLIdentifier string `json:"slug"`
}

//ReadArticle response model
type ReadArticle struct {
	Article *model.Article `json:"article"`
}

//HomeArticles response model
type HomeArticles struct {
	Articles      []*model.Article `json:"articles"`
	TotalArticles int              `json:"totalArticles"`
}

//UpdateArticle response model
type UpdateArticle struct {
	URLIdentifier string `json:"slug"`
}

//GetArticleFilters response model
type GetArticleFilters struct {
	Tags    []string `json:"tags"`
	Authors []string `json:"authors"`
}
