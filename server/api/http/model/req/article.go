package req

import (
	"server/model"
)

//CreateArticle request model
type CreateArticle struct {
	Article *model.Article `json:"article"`
}

//UpdateArticle request model
type UpdateArticle struct {
	Article *model.Article `json:"article"`
}
