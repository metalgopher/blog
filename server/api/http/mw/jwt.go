package mw

import (
	"context"
	"net/http"
	"server/env"
	"strings"

	"server/api/http/model/res"
	"server/api/http/util/auth"
	"server/loggers"
	"server/model"

	"github.com/dgrijalva/jwt-go/v4"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

// Context keys
var (
	JWTCtxKey = &contextKey{"JWT"}
)

type contextKey struct {
	name string
}

func (k *contextKey) String() string {
	return k.name
}

//Authenticate returns jwtParse middleware and authenticates via JWT
func Authenticate() func(next http.Handler) http.Handler {
	return jwtParse(env.GetConfig().AuthJWTSigningKey, 0)
}

//Authorize returns jwtParse middleware, authenticates via jwt, and authorizes based on minimum role
func Authorize(requiredRole model.AcctRole) func(next http.Handler) http.Handler {
	return jwtParse(env.GetConfig().AuthJWTSigningKey, requiredRole)
}

//protects routes by parsing/validating Authorization header (Bearer <token>) or query param "token"
func jwtParse(signingKey []byte, requiredRole model.AcctRole) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			//get token from req header
			var rawJWT string
			authHeaderParts := strings.Split(r.Header.Get("Authorization"), " ")
			if len(authHeaderParts) == 2 && strings.ToLower(authHeaderParts[0]) == "bearer" {
				rawJWT = authHeaderParts[1]
			} else if r.URL.Query().Get("token") != "" {
				rawJWT = r.URL.Query().Get("token")
			} else {
				jwtAuthFailed(w, r)
				return
			}

			//parse jwt
			parsedJWT, err := jwt.ParseWithClaims(rawJWT, &auth.UserClaimsJWT{}, func(token *jwt.Token) (interface{}, error) {
				return signingKey, nil
			})
			if err != nil {
				loggers.GetJSONLogger().WithField("token", rawJWT).WithError(err).Trace("error parsing claims")
				jwtAuthFailed(w, r)
				return
			}

			//validate claims
			var jwtClaims *auth.UserClaimsJWT
			if claims, ok := parsedJWT.Claims.(*auth.UserClaimsJWT); ok && parsedJWT.Valid && claims.Email != "" && claims.Username != "" && claims.Role.Valid() {
				jwtClaims = claims
			} else {
				loggers.GetJSONLogger().WithField("token", rawJWT).WithError(err).Trace("invalid claims")
				jwtAuthFailed(w, r)
				return
			}

			//authorize
			if jwtClaims.Role < requiredRole {
				loggers.GetJSONLogger().WithField("token", rawJWT).WithError(err).Trace("invalid claims")
				jwtAuthFailed(w, r)
				return
			}

			//store jwtClaims in request context
			ctx := r.Context()
			next.ServeHTTP(w, r.WithContext(context.WithValue(ctx, JWTCtxKey, jwtClaims)))
		})
	}
}

func jwtAuthFailed(w http.ResponseWriter, r *http.Request) {
	logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["req_id"] = reqID
	}

	resCode := http.StatusUnauthorized
	resMsg := "Not authorized"
	logFields["code"] = resCode
	loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
	render.Status(r, resCode)
	render.JSON(w, r, res.SimpleError{Err: resMsg})
}
