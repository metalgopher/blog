package account

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"time"

	"server/api/http/model/req"
	"server/api/http/model/res"
	"server/api/http/mw"
	"server/api/http/util/auth"
	"server/db"
	"server/email"
	"server/env"
	"server/interfaces"
	"server/loggers"
	"server/model"

	"github.com/dgrijalva/jwt-go/v4"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

//GetAvailableUsername returns whether username is valid and available
func GetAvailableUsername() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		//parse authorization basic header
		var reqUname string
		if uname, _, ok := r.BasicAuth(); ok {
			reqUname = uname
		} else {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//Username format
		unameRegex := regexp.MustCompile("^[a-zA-Z0-9]{3,20}$")
		if !unameRegex.MatchString(reqUname) {
			resCode := http.StatusBadRequest
			resMsg := "Invalid Username"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("uname", reqUname).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//check db
		acct, _ := db.GetAccountByUsername(reqUname, nil)

		//send response with bool (whether avail or not)
		resCode := http.StatusOK
		resMsg := "username availability"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).WithField("isAvail", acct == nil).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.UsernameAvailability{IsAvailable: acct == nil})
	}
}

//PostRegister returns handler for account registration POST request
func PostRegister(client interfaces.HTTPClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		//read request body
		reqBody, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//parse request body
		var registerReq req.Register
		if err := json.Unmarshal(reqBody, &registerReq); err != nil {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- validate request --

		//parse authorization basic header
		var reqUname, reqEmail string
		if uname, email, ok := r.BasicAuth(); ok {
			reqUname, reqEmail = uname, email
		} else {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//Username format
		unameRegex := regexp.MustCompile("^[a-zA-Z0-9]{3,20}$")
		if !unameRegex.MatchString(reqUname) {
			resCode := http.StatusBadRequest
			resMsg := "Invalid Username"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//Email format
		emailRegex := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
		if !emailRegex.MatchString(reqEmail) {
			resCode := http.StatusBadRequest
			resMsg := "Invalid Email"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//reCAPTCHA token is present
		if registerReq.ReCaptchaToken == "" {
			resCode := http.StatusBadRequest
			resMsg := "Missing reCAPTCHA token"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//Check if username exists
		if acct, _ := db.GetAccountByUsername(reqUname, nil); acct != nil {
			// account already found
			resCode := http.StatusConflict
			resMsg := "uname"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//Check if account exists
		if acct, _ := db.GetAccountByEmail(reqEmail, nil); acct != nil {
			// account already found
			resCode := http.StatusConflict
			resMsg := "email"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- reCAPTCHA token validation --

		//create verification request
		recaptchaReq, _ := http.NewRequest("POST", fmt.Sprintf("%s?secret=%s&response=%s", env.GetConfig().ProviderRecaptchaEndpointVerify, env.GetConfig().ProviderRecaptchaSecretKey, registerReq.ReCaptchaToken), nil)
		recaptchaReq.Header.Add("Content-Type", "application/json")

		//send verification request
		rawRes, recaptchaErr := client.Do(recaptchaReq)
		if recaptchaErr != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error occurred"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", recaptchaErr).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//read verification response
		rawResBody, _ := ioutil.ReadAll(rawRes.Body)
		defer rawRes.Body.Close()
		var recaptchaRes res.RecaptchaVerify
		json.Unmarshal(rawResBody, &recaptchaRes)

		//validate verification result
		if !recaptchaRes.Success {
			resCode := http.StatusUnauthorized
			resMsg := "Not authorized"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- process registration --

		//create jwt claims
		claims := auth.UserClaimsJWT{Username: reqUname, Email: reqEmail, Role: model.AcctRoleMember}
		claims.StandardClaims = jwt.StandardClaims{
			ExpiresAt: jwt.NewTime(float64(time.Now().Unix()) + env.GetConfig().AuthJWTAcctVerifyExpSeconds),
			Issuer:    env.GetConfig().AuthJWTIssuer,
		}
		token := jwt.NewWithClaims(env.GetConfig().AuthJWTSigningAlgo, claims)
		signedToken, _ := token.SignedString(env.GetConfig().AuthJWTSigningKey)

		//create email md5 hash
		hashBytes := md5.Sum([]byte(reqEmail))
		emailHash := hex.EncodeToString(hashBytes[:])

		//register account in db with jwt
		if err := db.RegisterNewAccount(reqEmail, string(emailHash), reqUname, signedToken, model.AcctRoleMember, nil); err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error occurred"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//send account verification email
		regLink := fmt.Sprintf("%s://%s/register/verify?token=%s", env.GetConfig().AppHostProtocol, env.GetConfig().AppHostName, signedToken)
		emailData := email.AcctVerifyTemplateData{RegLink: regLink}
		emailData.AppName = env.GetConfig().AppName
		emailData.CompanyName = env.GetConfig().AppCompany
		emailData.CompanyLink = env.GetConfig().AppCompanyLink
		emailData.CopyrightYear = time.Now().Year()
		if resCode, err := email.EmailProvider.SendTXAccountVerify(reqEmail, env.GetConfig().AuthLoginEmailFrom, "Please Verify Your Account", emailData, client, nil); err != nil {
			logFields["code"] = resCode
			resMsg := "Error occurred"
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//send success message
		resCode := http.StatusCreated
		resMsg := "Verification pending"
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.SimpleMsg{Msg: resMsg})
		return
	}
}

//PostRegisterResendVerification resends the email to verify a registered, unverified account
func PostRegisterResendVerification(client interfaces.HTTPClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		//parse authorization basic header
		var reqEmailHash string
		if emailHash, _, ok := r.BasicAuth(); ok {
			reqEmailHash = emailHash
		} else {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- validate request --

		//Check if account exists
		acct, _ := db.GetAccountByEmailHash(reqEmailHash, nil)
		if acct == nil {
			// account already found
			resCode := http.StatusNotFound
			resMsg := "Account not found"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}
		if acct.VerifiedAt != nil {
			// account already found
			resCode := http.StatusConflict
			resMsg := "Already verified"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- account found & unverified, proceed with verify email request --

		//create jwt claims
		claims := auth.UserClaimsJWT{Username: acct.Username, Email: acct.Email, Role: model.AcctRoleMember}
		claims.StandardClaims = jwt.StandardClaims{
			ExpiresAt: jwt.NewTime(float64(time.Now().Unix()) + env.GetConfig().AuthJWTAcctVerifyExpSeconds),
			Issuer:    env.GetConfig().AuthJWTIssuer,
		}
		token := jwt.NewWithClaims(env.GetConfig().AuthJWTSigningAlgo, claims)
		signedToken, _ := token.SignedString(env.GetConfig().AuthJWTSigningKey)

		//update account jwt for new verification email
		if err := db.UpdateAccountJWT(acct.Email, signedToken, nil); err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error occurred"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//send account verification email
		regLink := fmt.Sprintf("%s://%s/register/verify?token=%s", env.GetConfig().AppHostProtocol, env.GetConfig().AppHostName, signedToken)
		emailData := email.AcctVerifyTemplateData{RegLink: regLink}
		emailData.AppName = env.GetConfig().AppName
		emailData.CompanyName = env.GetConfig().AppCompany
		emailData.CompanyLink = env.GetConfig().AppCompanyLink
		emailData.CopyrightYear = time.Now().Year()
		if resCode, err := email.EmailProvider.SendTXAccountVerify(acct.Email, env.GetConfig().AuthLoginEmailFrom, "Please Verify Your Account", emailData, client, nil); err != nil {
			logFields["code"] = resCode
			resMsg := "Error occurred"
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//verification email resent successfully
		resCode := http.StatusOK
		resMsg := "Verification email sent successfully"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.SimpleMsg{Msg: resMsg})
	}
}

//GetRegisterVerify returns handler for new account verification
func GetRegisterVerify() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		//validate that jwt is present in query
		signedToken := r.URL.Query().Get("token")
		if signedToken == "" {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//read claims from context (written by mw)
		jwtClaims := r.Context().Value(mw.JWTCtxKey).(*auth.UserClaimsJWT)

		//check if account exists
		acct, _ := db.GetAccountByEmail(jwtClaims.Email, nil)
		if acct == nil {
			// account not found
			logFields["code"] = http.StatusNotFound
			resMsg := "Not found"
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, http.StatusNotFound)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//check that account is not verified
		if acct.VerifiedAt != nil {
			// already verified
			logFields["code"] = http.StatusConflict
			resMsg := "Already verified"
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, http.StatusConflict)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//check that account jwt matches
		if *acct.JWT != signedToken {
			logFields["code"] = http.StatusConflict
			resMsg := "Conflict"
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, http.StatusConflict)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- valid account verification --

		//process verify new account in db
		if err := db.RegisterNewAccountVerify(jwtClaims.Email, signedToken, nil); err != nil {
			resCode := http.StatusConflict
			resMsg := "Error occurred"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//account verified successfully
		resCode := http.StatusOK
		resMsg := "Account verified successfully"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.SimpleMsg{Msg: resMsg})
	}
}
