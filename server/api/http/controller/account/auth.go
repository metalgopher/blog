package account

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"server/api/http/model/res"
	"server/api/http/mw"
	"server/api/http/util/auth"
	"server/db"
	"server/email"
	"server/env"
	"server/interfaces"
	"server/loggers"

	"github.com/dgrijalva/jwt-go/v4"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

//PostLogin returns handler for initial login request via email auth code
func PostLogin(client interfaces.HTTPClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		// -- validate request authorization header --
		var reqEmailHash string
		if emailHash, _, ok := r.BasicAuth(); ok {
			reqEmailHash = emailHash
		} else {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- process request --

		//ensure account exists
		acct, _ := db.GetAccountByEmailHash(reqEmailHash, nil)
		if acct == nil {
			resCode := http.StatusNotFound
			resMsg := "Account not found"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//resend account verification email if not verified
		if acct.VerifiedAt == nil {
			//send success message
			resCode := http.StatusConflict
			resMsg := "Account not verified"
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleMsg{Msg: resMsg})
			return
		}

		//verified account exists -- generate auth code
		authCode, _ := auth.GenerateNumericCodeString(env.GetConfig().AuthLoginCodeDigits, env.GetConfig().AuthLoginCodeSpaceIndexes, time.Now().UnixNano())

		//save auth code with expiry
		if err := db.SaveAccountAuthCode(acct.Email, strings.ReplaceAll(strings.TrimSpace(authCode), " ", ""), time.Now().Add(time.Second*time.Duration(env.GetConfig().AuthLoginCodeTTLSeconds)), nil); err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error occurred"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//send auth code email
		emailData := email.LoginAuthCodeTemplateData{AuthCode: authCode}
		emailData.AppName = env.GetConfig().AppName
		emailData.CompanyName = env.GetConfig().AppCompany
		emailData.CompanyLink = env.GetConfig().AppCompanyLink
		emailData.CopyrightYear = time.Now().Year()
		if resCode, err := email.EmailProvider.SendTXLoginAuthCode(acct.Email, env.GetConfig().AuthLoginEmailFrom, "Here's Your One-Time Login Code", emailData, client, nil); err != nil {
			logFields["code"] = resCode
			resMsg := "Error occurred"
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//send success message
		resCode := http.StatusOK
		resMsg := "Request received"
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.SimpleMsg{Msg: resMsg})
		return
	}
}

//PostLoginVerify returns handler for login auth code verification request, issues jwt
func PostLoginVerify() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		// -- validate request authorization header --
		var reqEmailHash, reqAuthCode string
		if emailHash, authCode, ok := r.BasicAuth(); ok {
			reqEmailHash, reqAuthCode = emailHash, authCode
		} else {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- process request --

		//ensure account exists and is verified and auth code/expiry exists
		acct, _ := db.GetAccountByEmailHash(reqEmailHash, nil)
		if acct == nil || acct.VerifiedAt == nil || acct.AuthCode == nil || acct.AuthCodeExp == nil {
			resCode := http.StatusUnauthorized
			resMsg := "Not authorized"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//ensure auth code matches
		if *acct.AuthCode != strings.ReplaceAll(strings.TrimSpace(reqAuthCode), " ", "") {
			resCode := http.StatusUnauthorized
			err := res.ErrorCodeInvalidCreds.GetSimpleErr()
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(err.Err)
			render.Status(r, resCode)
			render.JSON(w, r, err)
			return
		}

		//ensure account auth code is not expired
		if acct.AuthCodeExp.Before(time.Now()) {
			resCode := http.StatusUnauthorized
			err := res.ErrorCodeExpiredCreds.GetSimpleErr()
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(err.Err)
			render.Status(r, resCode)
			render.JSON(w, r, err)
			return
		}

		// -- valid login --

		//create jwt claims
		claims := auth.UserClaimsJWT{Username: acct.Username, Email: acct.Email, Role: acct.Role}
		claims.StandardClaims = jwt.StandardClaims{
			ExpiresAt: jwt.NewTime(float64(time.Now().Unix()) + env.GetConfig().AuthJWTLoginExpSeconds),
			Issuer:    env.GetConfig().AuthJWTIssuer,
		}
		token := jwt.NewWithClaims(env.GetConfig().AuthJWTSigningAlgo, claims)
		signedToken, _ := token.SignedString(env.GetConfig().AuthJWTSigningKey)

		//update account in db with new jwt and remove auth code values
		if err := db.LoginAccount(acct.Email, signedToken, nil); err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error occurred"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//send response with jwt
		resCode := http.StatusOK
		resMsg := "Login successful"
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.Login{Username: acct.Username, Email: acct.Email, Role: acct.Role.ClientName(), JWT: signedToken})
		return
	}
}

//GetIdentify returns the email and role for the current JWT
func GetIdentify() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		// -- process request --

		//read claims from context (written by mw)
		jwtClaims := r.Context().Value(mw.JWTCtxKey).(*auth.UserClaimsJWT)

		role := strconv.Itoa(int(jwtClaims.Role))

		//send response with role
		resCode := http.StatusOK
		resMsg := "Identity retrieved"
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.GetIdentify{Username: jwtClaims.Username, Role: env.GetConfig().AuthRoleNameMap[role]})
		return
	}
}
