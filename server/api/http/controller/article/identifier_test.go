package article_test

import (
	"testing"

	"server/api/http/controller/article"
)

var generateURLIdentifierTests = []struct {
	input              string
	expectedIdentifier string
}{
	{
		input:              "The quick brown fox... jumped over the lazy dog.  OMG!",
		expectedIdentifier: "the-quick-brown-fox-jumped-over-the-lazy-dog-omg",
	},
	{
		input:              "so-money",
		expectedIdentifier: "so-money",
	},
	{
		input:              "So-money $$$!!!",
		expectedIdentifier: "so-money",
	},
}

func TestCreateIdentifier(t *testing.T) {
	for idx, tt := range generateURLIdentifierTests {
		actualIdentifier := article.CreateIdentifier(tt.input)

		if actualIdentifier != tt.expectedIdentifier {
			t.Errorf("TestGenerateURLIdentifier(%d): expected %s, actual %s", idx, tt.expectedIdentifier, actualIdentifier)
		}
	}
}
