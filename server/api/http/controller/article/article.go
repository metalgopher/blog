package article

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"server/api/http/model/req"
	"server/api/http/model/res"
	"server/api/http/mw"
	"server/api/http/util/auth"
	"server/db"
	"server/loggers"
	"server/model"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

//PostCreateArticle creates new article
func PostCreateArticle() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		//read request body
		reqBody, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//parse request body
		var articleReq req.CreateArticle
		if err := json.Unmarshal(reqBody, &articleReq); err != nil {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//validate request body

		// -- title
		articleReq.Article.Title = strings.TrimSpace(articleReq.Article.Title)
		if len([]rune(articleReq.Article.Title)) < 3 {
			resCode := http.StatusBadRequest
			resMsg := "Title too short"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- markdown
		articleReq.Article.Markdown = strings.TrimSpace(articleReq.Article.Markdown)
		if len([]rune(articleReq.Article.Markdown)) < 3 || len([]rune(articleReq.Article.Markdown)) > 65535 {
			resCode := http.StatusBadRequest
			resMsg := "Invalid content length, must be 3 to 65,535 characters"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- tags
		if articleReq.Article.TagNames != nil {
			for _, tagName := range articleReq.Article.TagNames {
				if len(strings.TrimSpace(tagName)) < 3 {
					resCode := http.StatusBadRequest
					resMsg := "Invalid tag length, must be 3 or more characters"
					logFields["code"] = resCode
					loggers.GetJSONLogger().WithFields(logFields).WithField("tagName", tagName).Trace(resMsg)
					render.Status(r, resCode)
					render.JSON(w, r, res.SimpleError{Err: resMsg})
					return
				}
			}
		}

		// -- request body is valid --

		//read claims from context (written by mw)
		jwtClaims := r.Context().Value(mw.JWTCtxKey).(*auth.UserClaimsJWT)

		//set author email
		articleReq.Article.AuthorUname = jwtClaims.Username

		//generate unique url identifier
		articleReq.Article.URLIdentifier = generateURLIdentifier(articleReq.Article)

		//transform from view model
		articleReq.Article.FromView()

		//set article tags
		if articleReq.Article.TagNames != nil {
			articleReq.Article.Tags = make(map[string]model.Tag)
			for _, tagName := range articleReq.Article.TagNames {
				tag := createIdentifier(tagName)
				articleReq.Article.Tags[tag] = model.Tag{DisplayName: tag}
			}
		}

		createdArticle, err := db.CreateArticle(articleReq.Article, nil)
		if err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error adding article"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithError(err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//article created successfully

		resCode := http.StatusCreated
		resMsg := "Article created"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.CreateArticle{URLIdentifier: createdArticle.URLIdentifier})
	}
}

//GetReadArticle retrieves article from db
func GetReadArticle() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		urlIdentifier := chi.URLParam(r, "urlIdentifier")

		// loggers.GetJSONLogger().WithField("urlIdentifier", urlIdentifier).Debug("request param")

		// -- get article from db --
		article, err := db.GetArticleByURLIdentifier(urlIdentifier, nil)
		if err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error retrieving article"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithError(err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		if article == nil {
			resCode := http.StatusNotFound
			resMsg := "Not found"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- article found --

		//prepare for view
		article.ToView()

		//success response
		resCode := http.StatusOK
		resMsg := "Article found"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.ReadArticle{Article: article})
	}
}

//GetHomeArticles retrieves articles for home view
func GetHomeArticles() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		// -- validate request --
		var pageNumber int
		page := r.URL.Query().Get("page")
		if val, err := strconv.Atoi(page); err == nil && val >= 1 {
			pageNumber = val
		} else {
			resCode := http.StatusBadRequest
			resMsg := "missing/invalid articles page"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		var articlesPerPage int
		pageSize := r.URL.Query().Get("pageSize")
		if val, err := strconv.Atoi(pageSize); err == nil && (val == 5 || val == 10 || val == 25) {
			articlesPerPage = val
		} else {
			if pageSize != "" {
				resCode := http.StatusBadRequest
				resMsg := "invalid page size"
				logFields["code"] = resCode
				loggers.GetJSONLogger().WithFields(logFields).Error(resMsg)
				render.Status(r, resCode)
				render.JSON(w, r, res.SimpleError{Err: resMsg})
				return
			}
		}

		//default page size
		const articleCountPerPage int = 10
		if articlesPerPage == 0 {
			articlesPerPage = articleCountPerPage
		}

		// -- get request filters if any
		filterTags, filterAuthors, filterSearchTerms := r.URL.Query()["filterTag"], r.URL.Query()["filterAuthor"], r.URL.Query()["filterTerm"]

		//normalize filters
		for idx, tag := range filterTags {
			filterTags[idx] = strings.Join(regexp.MustCompile(`[^a-zA-Z0-9-]+`).Split(tag, -1), "")
		}
		for idx, author := range filterAuthors {
			filterAuthors[idx] = strings.Join(regexp.MustCompile(`[^a-zA-Z0-9]+`).Split(author, -1), "")
		}
		for idx, term := range filterSearchTerms {
			if regexp.MustCompile(`^".+"$`).MatchString(term) {
				filterSearchTerms[idx] = strings.Join(regexp.MustCompile(`[^a-zA-Z0-9\s-]+`).Split(term, -1), "")
			} else {
				filterSearchTerms[idx] = strings.Join(regexp.MustCompile(`[^a-zA-Z0-9]+`).Split(term, -1), "")
			}
		}
		//DEBUG
		// fmt.Printf("filterSearchTerms: %v\n", filterSearchTerms)

		// -- get article from db --
		articles, totalArticles, err := db.GetArticlesHome(filterTags, filterAuthors, filterSearchTerms, articlesPerPage, pageNumber, nil)
		if err != nil || articles == nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error retrieving article"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithError(err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- article found --

		//prepare for view
		for i := 0; i < len(articles); i++ {
			articles[i].ToView()
		}

		//success response
		resCode := http.StatusOK
		resMsg := "Articles found"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.HomeArticles{Articles: articles, TotalArticles: totalArticles})
	}
}

//PutUpdateArticle updates member emoji
func PutUpdateArticle() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		//read request body
		reqBody, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//parse request body
		var articleReq req.UpdateArticle
		if err := json.Unmarshal(reqBody, &articleReq); err != nil {
			resCode := http.StatusBadRequest
			resMsg := "Bad request"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//validate request body

		// -- title
		articleReq.Article.Title = strings.TrimSpace(articleReq.Article.Title)
		if len([]rune(articleReq.Article.Title)) < 3 {
			resCode := http.StatusBadRequest
			resMsg := "Title too short"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- markdown
		articleReq.Article.Markdown = strings.TrimSpace(articleReq.Article.Markdown)
		if len([]rune(articleReq.Article.Markdown)) < 3 || len([]rune(articleReq.Article.Markdown)) > 65535 {
			resCode := http.StatusBadRequest
			resMsg := "Invalid content length, must be 3 to 65,535 characters"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithField("err", err).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- urlIdentifier
		urlIdentifier := chi.URLParam(r, "urlIdentifier")

		// -- validate presence in db
		article, err := db.GetArticleByURLIdentifier(urlIdentifier, nil)
		if err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error updating article"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithError(err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}
		if article == nil {
			resCode := http.StatusNotFound
			resMsg := "Not found"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- authorization

		//read claims from context (written by mw)
		jwtClaims := r.Context().Value(mw.JWTCtxKey).(*auth.UserClaimsJWT)
		if jwtClaims.Username != article.AuthorUname && jwtClaims.Role < model.AcctRoleAdmin {
			resCode := http.StatusUnauthorized
			resMsg := "Not authorized"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		// -- request body is valid --

		//update urlIdentifier
		updatedURLID := createIdentifier(articleReq.Article.Title)
		if article.URLIdentifier == updatedURLID {
			articleReq.Article.URLIdentifier = article.URLIdentifier
		} else {
			articleReq.Article.URLIdentifier = generateURLIdentifier(articleReq.Article)
		}

		//transform from view model
		articleReq.Article.FromView()

		//update id
		articleReq.Article.ID = article.ID

		//normalize tags to identifiers
		if articleReq.Article.Tags != nil {
			idTags := make(map[string]model.Tag)
			for tag := range articleReq.Article.Tags {
				idTags[tag] = model.Tag{DisplayName: createIdentifier(tag)}
			}
			articleReq.Article.Tags = idTags
		}

		if err := db.UpdateArticle(articleReq.Article, nil); err != nil {
			resCode := http.StatusInternalServerError
			resMsg := "Error adding article"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithError(err).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			return
		}

		//article updated successfully
		resCode := http.StatusOK
		resMsg := "Article updated"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, res.UpdateArticle{URLIdentifier: articleReq.Article.URLIdentifier})
	}
}

//GetArticleFilters retrieves articles for home view
func GetArticleFilters() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logFields := logrus.Fields{"ts": loggers.GetTimestamp()}

		if reqID := middleware.GetReqID(r.Context()); reqID != "" {
			logFields["req_id"] = reqID
		}

		// -- get filters from db --
		filtersResponse := &res.GetArticleFilters{}

		wg := &sync.WaitGroup{}
		wg.Add(2)
		errs := make(chan error, 2)

		//authors
		go func(out *res.GetArticleFilters) {
			authors, err := db.GetAllAuthors(nil)
			if err != nil {
				errs <- err
			}

			out.Authors = authors

			//done
			wg.Done()
		}(filtersResponse)

		//tags
		go func(out *res.GetArticleFilters) {
			tags, err := db.GetAllTags(nil)
			if err != nil {
				errs <- err
			}

			out.Tags = tags

			//done
			wg.Done()
		}(filtersResponse)

		//let db calls complete
		wg.Wait()

		//handle err(s)
		if len(errs) > 0 {
			resCode := http.StatusInternalServerError
			resMsg := "Error retrieving filters"
			logFields["code"] = resCode
			loggers.GetJSONLogger().WithFields(logFields).WithError(<-errs).Error(resMsg)
			render.Status(r, resCode)
			render.JSON(w, r, res.SimpleError{Err: resMsg})
			close(errs)
			return
		}

		// -- filters found --

		//success response
		resCode := http.StatusOK
		resMsg := "Filters found"
		logFields["code"] = resCode
		loggers.GetJSONLogger().WithFields(logFields).Trace(resMsg)
		render.Status(r, resCode)
		render.JSON(w, r, filtersResponse)
	}
}
