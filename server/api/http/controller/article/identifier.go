package article

import (
	"fmt"
	"regexp"
	"strings"

	"server/db"
	"server/model"
)

func generateURLIdentifier(article *model.Article) string {
	// -- create identifier from title --
	identifier := createIdentifier(article.Title)

	// -- check if identifier exists for another article, add incremental counter until unique
	isUnique, counter := false, ""
	for i := 0; !isUnique; i++ {
		if i > 0 {
			counter = fmt.Sprintf("-%d", i)
		}
		result, err := db.GetArticleByURLIdentifier(fmt.Sprintf("%s%s", identifier, counter), nil)
		if err != nil {
			continue
		}

		if result == nil {
			isUnique = true
			if len(counter) > 0 {
				identifier = fmt.Sprintf("%s%s", identifier, counter)
			}
		}
	}

	return identifier
}

func createIdentifier(input string) string {
	//remove characters from title which are not letters
	identifier := strings.Join(regexp.MustCompile(`[^a-zA-Z0-9]+`).Split(strings.TrimSpace(strings.ToLower(input)), -1), "-")
	if identifier[len(identifier)-1] == '-' {
		identifier = identifier[:len(identifier)-1]
	}

	return identifier
}
