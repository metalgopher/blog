package http

import (
	"net/http"
	"time"

	accountController "server/api/http/controller/account"
	articleController "server/api/http/controller/article"
	"server/api/http/model/res"
	"server/api/http/mw"
	"server/loggers"
	"server/model"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

//GetRouter returns the application HTTP router
func GetRouter() chi.Router {
	r := chi.NewRouter()

	// A good base middleware stack
	r.Use(middleware.RealIP)
	r.Use(middleware.RequestID)
	// r.Use(middleware.Logger)
	r.Use(loggers.NewStructuredLogger(loggers.GetJSONLogger()))
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	r.Use(middleware.Timeout(60 * time.Second))

	// Basic CORS
	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	//health check
	r.Get("/health", func(w http.ResponseWriter, r *http.Request) {
		render.Status(r, http.StatusOK)
		response := res.HealthCheck{Msg: "OK"}
		render.JSON(w, r, response)
		loggers.GetJSONLogger().WithFields(logrus.Fields{"msg": response.Msg, "ts": loggers.GetTimestamp()}).Info("Health Check OK")
	})

	//main api router
	r.Mount("/api", apiRouter())

	return r
}

func apiRouter() chi.Router {
	r := chi.NewRouter()

	//account router
	r.Mount("/account", accountRouter())

	//article router
	r.Mount("/article", articleRouter())

	return r
}

//returns account router
func accountRouter() chi.Router {
	r := chi.NewRouter()
	httpClient := &http.Client{}

	//verify if username is available
	r.Get("/register/uname/availability", accountController.GetAvailableUsername())

	//new account registration
	r.Post("/register", accountController.PostRegister(httpClient))

	//new account verification after initial registration request
	r.With(mw.Authenticate()).Get("/register/verify", accountController.GetRegisterVerify())

	//resend account verification email
	r.Post("/register/verify/resend", accountController.PostRegisterResendVerification(httpClient))

	//account login request to receive email auth code
	r.Post("/login", accountController.PostLogin(httpClient))

	//account login verification of email auth code, issues jwt
	r.Post("/login/verify", accountController.PostLoginVerify())

	//retrieve auth role from JWT
	r.With(mw.Authenticate()).Get("/identity", accountController.GetIdentify())

	return r
}

//returns article router
func articleRouter() chi.Router {
	r := chi.NewRouter()

	//get article
	r.Get(`/view/{urlIdentifier:[a-z\d-]{3,}}`, articleController.GetReadArticle())

	//creates new article
	r.With(mw.Authorize(model.AcctRoleAuthor)).Post("/", articleController.PostCreateArticle())

	//update article
	r.With(mw.Authorize(model.AcctRoleAuthor)).Put(`/edit/{urlIdentifier:[a-z\d-]{3,}}`, articleController.PutUpdateArticle())

	r.Get("/home", articleController.GetHomeArticles())

	r.Get("/filters", articleController.GetArticleFilters())

	return r
}
