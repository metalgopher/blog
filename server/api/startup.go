package api

import (
	"fmt"
	"log"
	"net/http"
	"regexp"

	apiHTTP "server/api/http"
	"server/db"
	"server/email"
	"server/env"
	"server/loggers"

	"github.com/sirupsen/logrus"
)

var (
	jsonLogger *logrus.Logger
	logger     *log.Logger
)

//DisplayTitle shows the ASCII art title
func DisplayTitle() {
	fmt.Println(`

                  ███████████████████████
              ████                       █████                
            ██                               ███              
           ██   ┏━━━┳┓        ┏┳━━┓     ┏┓     ███           
           ██   ┃┏━┓┃┃        ┃┃┏┓┃     ┃┃       ██        
        ████    ┃┃ ┗┫┃┏━━┳┓┏┳━┛┃┗┛┗┳━━┳━┫┃┏┓     ██        
   █████▒▒▒     ┃┃ ┏┫┃┃┏┓┃┃┃┃┏┓┃┏━┓┃┏┓┃┏┫┗┛┛      ██ 
 ██▒▒           ┃┗━┛┃┗┫┗┛┃┗┛┃┗┛┃┗━┛┃┏┓┃┃┃┏┓┓        ███  
 ██▒▒           ┗━━━┻━┻━━┻━━┻━━┻━━━┻┛┗┻┛┗┛┗┛          ██
  ██▒▒▒                                              ▒██
   ██▒▒▒▒        © Cloudbark.com 2020              ▒██
    ████████▓▓███████████████████████████████████████  
 
	`)
}

//StartupTasks executes all tasks to perfor before server starts listening for requests
func StartupTasks() {
	logger = loggers.GetLogger()

	logger.Printf("Server Startup <<\n\n")
	fmt.Printf(">> Running startup tasks...\n\n")

	// -- define all possible tasks
	task1 := dbConnect
	task2 := dbCreateSchemaTables
	task3 := dbQueries
	task4 := dbScripts
	task5 := initProviders
	task6 := initLogger

	// -- load required tasks --
	tasks := []*func(int, int){
		&task1,
		&task2,
		&task3,
		&task5,
		&task6,
	}

	// -- schedule optional tasks
	if len(env.GetConfig().DBScripts) > 0 {
		//schedule task 4th
		tasks = append(tasks, nil)
		copy(tasks[3:], append([]*func(int, int){&task4}, tasks[3:]...))
	}

	// -- run tasks --
	for i, task := range tasks {
		switch task {
		case nil:
			continue
		default:
			(*task)(i+1, len(tasks))
		}
	}

	//success
	logger.Printf("Startup tasks complete! <<\n\n")
}

// -- DB -- connecting or creating db if not exists
func dbConnect(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) DB -- connecting or creating db if not exists...\n", currentTask, totalTasks)
	if err := db.CreateSchema(nil); err == nil {
		logger.Printf("...SUCCESS!\n\n")
	} else {
		logger.Println("...FAIL!")
		panic(fmt.Sprintf("Startup task %d failed! >> %s", currentTask, err))
	}
}

// -- DB -- creating tables if not exists
func dbCreateSchemaTables(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) DB -- creating tables if not exist...", currentTask, totalTasks)
	if _, err := db.ExecScript("db", "sql", "create_tables.sql"); err == nil {
		logger.Printf("...SUCCESS!\n\n")
	} else {
		logger.Println("...FAIL!")
		panic(fmt.Sprintf("Startup task %d failed! >> %s", currentTask, err))
	}
}

// -- DB -- run startup scripts
func dbQueries(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) DB -- run startup queries...", currentTask, totalTasks)

	//insert roles
	if err := db.InsertRoles(nil); err != nil {
		logger.Println("...FAIL!")
		panic(fmt.Sprintf("Startup task %d failed! >> %s", currentTask, err))
	}

	logger.Printf("...SUCCESS!\n\n")
}

// -- DB -- run startup scripts
func dbScripts(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) DB -- run startup scripts...", currentTask, totalTasks)
	csep := regexp.MustCompile(`[\s+,]+`)
	scripts := csep.Split(env.GetConfig().DBScripts, -1)
	for _, scriptName := range scripts {
		if len(scriptName) == 0 {
			continue
		}
		if _, err := db.ExecScript("db", "sql", scriptName); err != nil {
			logger.Println("...FAIL!")
			panic(fmt.Sprintf("Startup task %d failed! >> %s", currentTask, err))
		}
	}
	logger.Printf("...SUCCESS!\n\n")
}

// -- Providers -- initialize providers
func initProviders(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) WS -- initialize service providers...", currentTask, totalTasks)

	//email
	switch env.GetConfig().ProviderEmailName {
	case "sendgrid":
		email.EmailProvider = email.SendGrid{
			ProviderEmailAuthHeader:     env.GetConfig().ProviderEmailAuthHeader,
			ProviderEmailEndpointSendTX: env.GetConfig().ProviderEmailEndpointSendTX,
		}
	}

	logger.Printf("...SUCCESS!\n\n")
}

// -- Logger -- initialize logger
func initLogger(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) Logger -- initialize logger...", currentTask, totalTasks)

	//logrus JSON logger
	loggers.InitLoggers(env.GetConfig().ServerLogLevel)

	logger.Printf("...SUCCESS!\n\n")
}

//Listen starts listening for incoming HTTP requests
func Listen() {
	logger.Printf("HTTP Server Listening on Port %s <<\n\n", env.GetConfig().ServerPort)
	http.ListenAndServe(fmt.Sprintf(":%s", env.GetConfig().ServerPort), apiHTTP.GetRouter())
}
