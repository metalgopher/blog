package loggers

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
)

var (
	//TimeFormat is the global log time format
	TimeFormat string = "2006-01-02T15:04:05.000"

	//logrus logger with JSON formatter
	jsonLogger *logrus.Logger

	//standard logger for general output
	general *log.Logger
)

//InitLoggers initializes loggers
func InitLoggers(lvl logrus.Level) {
	//json logger
	jsonLogger = logrus.New()
	jsonLogger.Formatter = &logrus.JSONFormatter{
		// disable, as we set our own
		DisableTimestamp: true,
	}
	//set level to see more/less logs
	jsonLogger.SetLevel(lvl)

	//general logger
	general = log.New(new(logger), "", log.Lmsgprefix)
}

//GetJSONLogger returns the json logger
func GetJSONLogger() *logrus.Logger {
	if jsonLogger == nil {
		InitLoggers(logrus.TraceLevel)
	}
	return jsonLogger
}

//GetLogger returns the general logger
func GetLogger() *log.Logger {
	if general == nil {
		InitLoggers(logrus.TraceLevel)
	}
	return general
}

//GetTimestamp returns a string representation of time.Now() using TimeFormat
func GetTimestamp() string {
	return time.Now().Format(TimeFormat)
}

type logger struct{}

func (l *logger) getTimestamp(timeFormat *string) string {
	var format string
	if timeFormat == nil || len(*timeFormat) == 0 {
		format = TimeFormat
	} else {
		format = *timeFormat
	}
	return time.Now().Format(format)
}

func (l *logger) Write(p []byte) (n int, err error) {
	timestamp := l.getTimestamp(nil)
	var sb strings.Builder
	sb.WriteString(timestamp)
	sb.WriteString(" >> ")
	sb.WriteString(string(p))
	return os.Stdout.WriteString(sb.String())
}

func (l *logger) WriteString(w io.Writer, s string) (n int, err error) {
	timestamp := l.getTimestamp(nil)
	var sb strings.Builder
	sb.WriteString(timestamp)
	sb.WriteString(" >> ")
	sb.WriteString(s)
	return io.WriteString(w, sb.String())
}

// StructuredLogger is a simple, but powerful implementation of a custom structured
// logger backed on logrus. I encourage users to copy it, adapt it and make it their
// own. Also take a look at https://github.com/pressly/lg for a dedicated pkg based
// on this work, designed for context-based http routers.

//NewStructuredLogger returns a RequestLogger middleware
func NewStructuredLogger(logger *logrus.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&StructuredLogger{Logger: logger})
}

//StructuredLogger is
type StructuredLogger struct {
	Logger *logrus.Logger
}

//NewLogEntry is
func (l *StructuredLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	entry := &StructuredLoggerEntry{Logger: logrus.NewEntry(l.Logger)}
	logFields := logrus.Fields{}

	logFields["ts"] = time.Now().Format(TimeFormat)

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["req_id"] = reqID
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	logFields["http_scheme"] = scheme
	logFields["http_proto"] = r.Proto
	logFields["http_method"] = r.Method

	logFields["remote_addr"] = r.RemoteAddr
	logFields["user_agent"] = r.UserAgent()

	logFields["uri"] = fmt.Sprintf("%s://%s%s", scheme, r.Host, r.RequestURI)

	entry.Logger = entry.Logger.WithFields(logFields)

	entry.Logger.Infoln("request started")

	return entry
}

//StructuredLoggerEntry is
type StructuredLoggerEntry struct {
	Logger logrus.FieldLogger
}

func (l *StructuredLoggerEntry) Write(status, bytes int, elapsed time.Duration) {
	fields := logrus.Fields{
		"resp_bytes_length": bytes,
		"resp_elapsed_ms":   float64(elapsed.Nanoseconds()) / 1000000.0,
	}
	if status != 0 {
		fields["resp_status"] = status
	}
	l.Logger = l.Logger.WithFields(fields)

	l.Logger.Infoln("request complete")
}

//Panic is
func (l *StructuredLoggerEntry) Panic(v interface{}, stack []byte) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"stack": string(stack),
		"panic": fmt.Sprintf("%+v", v),
	})
}

// Helper methods used by the application to get the request-scoped
// logger entry and set additional fields between handlers.
//
// This is a useful pattern to use to set state on the entry as it
// passes through the handler chain, which at any point can be logged
// with a call to .Print(), .Info(), etc.

//GetLogEntry is
func GetLogEntry(r *http.Request) logrus.FieldLogger {
	entry := middleware.GetLogEntry(r).(*StructuredLoggerEntry)
	return entry.Logger
}

//LogEntrySetField is
func LogEntrySetField(r *http.Request, key string, value interface{}) {
	if entry, ok := r.Context().Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
		entry.Logger = entry.Logger.WithField(key, value)
	}
}

//LogEntrySetFields is
func LogEntrySetFields(r *http.Request, fields map[string]interface{}) {
	if entry, ok := r.Context().Value(middleware.LogEntryCtxKey).(*StructuredLoggerEntry); ok {
		entry.Logger = entry.Logger.WithFields(fields)
	}
}
