package email

import "server/interfaces"

var (
	accountVerifyTemplateFileName = "email/templates/acctVerify.email"
	loginAuthCodeTemplateFileName = "email/templates/loginAuthCode.email"

	//EmailProvider is the provider for application emails
	EmailProvider Provider
)

//Provider is the interface for working with an email provider
type Provider interface {
	SendTXAccountVerify(string, string, string, AcctVerifyTemplateData, interfaces.HTTPClient, *string) (int, error)
	SendTXLoginAuthCode(string, string, string, LoginAuthCodeTemplateData, interfaces.HTTPClient, *string) (int, error)
}

//TemplateData is common data shared among all email templates
type TemplateData struct {
	AppName       string
	CompanyName   string
	CopyrightYear int
	CompanyLink   string
}

//AcctVerifyTemplateData wraps data for account verification emails
type AcctVerifyTemplateData struct {
	TemplateData
	RegLink string
}

//LoginAuthCodeTemplateData wraps data for account verification emails
type LoginAuthCodeTemplateData struct {
	TemplateData
	AuthCode string
}
