package email

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"strings"

	"server/interfaces"
)

//SendGrid is the interface for working with email service SendGrid
type SendGrid struct {
	ProviderEmailEndpointSendTX string
	ProviderEmailAuthHeader     string
}

//getSendTXEmailRequestBodyTemplate
func (sg SendGrid) getSendTXEmailRequestBody(to []string, from, subject, content string) string {
	builder := strings.Builder{}
	builder.WriteString(`{"personalizations": [{ "to": [`)

	for idx, toEmail := range to {
		var sep string
		if idx > 0 {
			sep = ","
		}
		builder.WriteString(fmt.Sprintf(`%s{ "email": "%s" }`, sep, toEmail))
	}

	builder.WriteString(fmt.Sprintf(`]}], "from": { "email": "%s" }, `, from))
	builder.WriteString(fmt.Sprintf(`"subject": "%s", `, subject))
	builder.WriteString(fmt.Sprintf(`"content": [{ "type": "text/html", "value": "%s"`, strings.ReplaceAll(strings.ReplaceAll(content, "\"", "\\\""), "\n", "")))
	builder.WriteString(`}]}`)

	return builder.String()
}

//SendTXAccountVerify is to send tx email to verify account/email
func (sg SendGrid) SendTXAccountVerify(recEmail, fromEmail, subject string, data AcctVerifyTemplateData, client interfaces.HTTPClient, templatePath *string) (int, error) {
	//read template
	var fileName string
	if templatePath == nil {
		fileName = accountVerifyTemplateFileName
	} else {
		fileName = *templatePath
	}
	emailTemplate, err := template.ParseFiles(fileName)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	//render template
	var renderedBytes bytes.Buffer
	emailTemplate.Execute(&renderedBytes, data)

	//prepare email request
	emailReqBody := sg.getSendTXEmailRequestBody([]string{recEmail}, fromEmail, subject, renderedBytes.String())

	// fmt.Println("body:", emailReqBody)

	mailReq, err := http.NewRequest("POST", sg.ProviderEmailEndpointSendTX, strings.NewReader(emailReqBody))
	mailReq.Header.Add("Authorization", sg.ProviderEmailAuthHeader)
	mailReq.Header.Add("Content-Type", "application/json")

	//send request
	res, resErr := client.Do(mailReq)
	if resErr != nil {
		return http.StatusInternalServerError, resErr
	}

	return res.StatusCode, nil
}

//SendTXLoginAuthCode is to send tx email with auth code used for login
func (sg SendGrid) SendTXLoginAuthCode(recEmail, fromEmail, subject string, data LoginAuthCodeTemplateData, client interfaces.HTTPClient, templatePath *string) (int, error) {
	//read template
	var fileName string
	if templatePath == nil {
		fileName = loginAuthCodeTemplateFileName
	} else {
		fileName = *templatePath
	}
	emailTemplate, err := template.ParseFiles(fileName)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	//render template
	var renderedBytes bytes.Buffer
	emailTemplate.Execute(&renderedBytes, data)

	//prepare email request
	emailReqBody := sg.getSendTXEmailRequestBody([]string{recEmail}, fromEmail, subject, renderedBytes.String())

	// fmt.Println("body:", emailReqBody)

	mailReq, err := http.NewRequest("POST", sg.ProviderEmailEndpointSendTX, strings.NewReader(emailReqBody))
	mailReq.Header.Add("Authorization", sg.ProviderEmailAuthHeader)
	mailReq.Header.Add("Content-Type", "application/json")

	//send request
	res, resErr := client.Do(mailReq)
	if resErr != nil {
		return http.StatusInternalServerError, resErr
	}

	return res.StatusCode, nil
}
