package email_test

import (
	"fmt"
	"net/http"
	"testing"

	"server/email"
)

type testClient struct {
	isFailer bool
}

func (tc testClient) Do(r *http.Request) (*http.Response, error) {
	if tc.isFailer {
		return nil, fmt.Errorf("Generated testing error")
	}
	return &http.Response{StatusCode: http.StatusAccepted}, nil
}

func TestSendTXAccountVerify(t *testing.T) {
	//template path
	templatePath := "templates/acctVerify.email"

	//set email provider
	var provider email.Provider
	provider = email.SendGrid{
		ProviderEmailAuthHeader:     "Bearer abc123xyz",
		ProviderEmailEndpointSendTX: "https://api.sendgrid.com/v3/mail/send",
	}

	//test data
	testData := email.AcctVerifyTemplateData{
		RegLink: "http://cloudbark.com",
	}

	testData.AppName = "CloudBark"
	testData.CompanyLink = "http://cloudbark.com"
	testData.CompanyName = "CloudBark"
	testData.CopyrightYear = 2020

	actualRescode, actualErr := provider.SendTXAccountVerify("user@example.com", "noreply@cloudbark.com", "A Testy Subject", testData, &testClient{}, &templatePath)
	if actualRescode != http.StatusAccepted {
		t.Errorf("TestSendTXAccountVerify >> expected response code: %d, actual: %d", http.StatusAccepted, actualRescode)
	}
	if actualErr != nil {
		t.Errorf("TestSendTXAccountVerify >> expected err: %v, actual: %v", nil, actualErr)
	}
}

func TestSendTXAccountVerifyClientError(t *testing.T) {
	//template path
	templatePath := "templates/acctVerify.email"

	//set email provider
	var provider email.Provider
	provider = email.SendGrid{
		ProviderEmailAuthHeader:     "Bearer abc123xyz",
		ProviderEmailEndpointSendTX: "https://api.sendgrid.com/v3/mail/send",
	}

	//test data
	testData := email.AcctVerifyTemplateData{
		RegLink: "http://cloudbark.com",
	}

	testData.AppName = "CloudBark"
	testData.CompanyLink = "http://cloudbark.com"
	testData.CompanyName = "CloudBark"
	testData.CopyrightYear = 2020

	actualRescode, actualErr := provider.SendTXAccountVerify("user@example.com", "noreply@cloudbark.com", "A Testy Subject", testData, &testClient{isFailer: true}, &templatePath)
	if actualRescode != http.StatusInternalServerError {
		t.Errorf("TestSendTXAccountVerify >> expected response code: %d, actual: %d", http.StatusInternalServerError, actualRescode)
	}
	if actualErr == nil {
		t.Errorf("TestSendTXAccountVerify >> expected err: %v, actual: %v", fmt.Errorf("Generated testing error"), actualErr)
	}
}

func TestSendTXLoginAuthCode(t *testing.T) {
	//template path
	templatePath := "templates/acctVerify.email"

	//set email provider
	var provider email.Provider
	provider = email.SendGrid{
		ProviderEmailAuthHeader:     "Bearer abc123xyz",
		ProviderEmailEndpointSendTX: "https://api.sendgrid.com/v3/mail/send",
	}

	//test data
	testData := email.LoginAuthCodeTemplateData{
		AuthCode: "123 456",
	}

	testData.AppName = "CloudBark"
	testData.CompanyLink = "http://cloudbark.com"
	testData.CompanyName = "CloudBark"
	testData.CopyrightYear = 2020

	actualRescode, actualErr := provider.SendTXLoginAuthCode("user@example.com", "noreply@cloudbark.com", "A Testy Subject", testData, &testClient{}, &templatePath)
	if actualRescode != http.StatusAccepted {
		t.Errorf("TestSendTXAccountVerify >> expected response code: %d, actual: %d", http.StatusAccepted, actualRescode)
	}
	if actualErr != nil {
		t.Errorf("TestSendTXAccountVerify >> expected err: %v, actual: %v", nil, actualErr)
	}
}

func TestSendTXLoginAuthCodeClientError(t *testing.T) {
	//template path
	templatePath := "templates/loginAuthCode.email"

	//set email provider
	var provider email.Provider
	provider = email.SendGrid{
		ProviderEmailAuthHeader:     "Bearer abc123xyz",
		ProviderEmailEndpointSendTX: "https://api.sendgrid.com/v3/mail/send",
	}

	//test data
	testData := email.LoginAuthCodeTemplateData{
		AuthCode: "123 456",
	}

	testData.AppName = "CloudBark"
	testData.CompanyLink = "http://cloudbark.com"
	testData.CompanyName = "CloudBark"
	testData.CopyrightYear = 2020

	actualRescode, actualErr := provider.SendTXLoginAuthCode("user@example.com", "noreply@cloudbark.com", "A Testy Subject", testData, &testClient{isFailer: true}, &templatePath)
	if actualRescode != http.StatusInternalServerError {
		t.Errorf("TestSendTXAccountVerify >> expected response code: %d, actual: %d", http.StatusInternalServerError, actualRescode)
	}
	if actualErr == nil {
		t.Errorf("TestSendTXAccountVerify >> expected err: %v, actual: %v", fmt.Errorf("Generated testing error"), actualErr)
	}
}

func TestGetSendTXEmailRequestBody(t *testing.T) {
	//email client
	sendGrid := email.SendGrid{}

	expected := `{"personalizations": [{ "to": [{ "email": "test@example.org" },{ "email": "test2@example.org" }]}], "from": { "email": "noreply@cloudbark.com" }, "subject": "A Testy Subject", "content": [{ "type": "text/html", "value": "<h1>Simple Header</h1>"}]}`
	actual := email.GetSendTXEmailRequestBody(sendGrid, []string{"test@example.org", "test2@example.org"}, "noreply@cloudbark.com", "A Testy Subject", "<h1>Simple Header</h1>")
	if actual != expected {
		t.Errorf("TestGetSendTXEmailRequestBody >> expected: %v, actual: %v", expected, actual)
	}
}
