INSERT INTO accounts(email, email_hash, username, role_id)
SELECT 'foo@cloudbark.com', 'foobarhash', 'mrfoo', '2'
WHERE NOT EXISTS (SELECT email FROM accounts WHERE email = 'foo@cloudbark.com');

INSERT INTO tags(display_name)
SELECT 'foo'
WHERE NOT EXISTS (SELECT display_name FROM tags WHERE display_name = 'foo');

INSERT INTO tags(display_name)
SELECT 'bar'
WHERE NOT EXISTS (SELECT display_name FROM tags WHERE display_name = 'bar');

INSERT INTO tags(display_name)
SELECT 'coolness'
WHERE NOT EXISTS (SELECT display_name FROM tags WHERE display_name = 'coolness');

INSERT INTO articles(url_identifier, author_uname, title, markdown)
SELECT 'a-foo-article', 'mrfoo', 'A Foo Article', 'This is only a test.<br/>Merely some foo to bar.'
WHERE NOT EXISTS (SELECT url_identifier FROM articles WHERE url_identifier = 'a-foo-article');

INSERT INTO article_tags(articleId, tag_name)
SELECT 1, 'foo'
WHERE NOT EXISTS (SELECT articleId FROM article_tags WHERE articleId = 1 AND tag_name = 'foo');

INSERT INTO article_tags(articleId, tag_name)
SELECT 1, 'bar'
WHERE NOT EXISTS (SELECT articleId FROM article_tags WHERE articleId = 1 AND tag_name = 'bar');

INSERT INTO article_tags(articleId, tag_name)
SELECT 1, 'coolness'
WHERE NOT EXISTS (SELECT articleId FROM article_tags WHERE articleId = 1 AND tag_name = 'coolness');
