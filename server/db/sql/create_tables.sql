create table if not exists account_roles (
    id int,
    role_name varchar(255) not null unique,
    primary key (id)
);

create table if not exists accounts (
	id int auto_increment,
    email varchar(255) not null unique,
    email_hash varchar(500) not null unique,
    username varchar(20) not null unique,
    role_id int not null,
    auth_code varchar(8),
    auth_code_exp datetime,
    jwt varchar(510),
    verified_at datetime,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (id),
    foreign key (role_id) references account_roles(id)
);

create table if not exists articles (
	id int auto_increment,
    url_identifier varchar(500) not null unique,
    author_uname varchar(20) not null,
    title varchar(255) not null,
    markdown text not null,
    text_content text not null,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (id),
    foreign key (author_uname) references accounts(username)
);

create table if not exists tags (
    display_name varchar(255),
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (display_name)
);

create table if not exists article_tags (
    articleId int,
    tag_name varchar(255),
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (articleId, tag_name),
    foreign key (articleId) references articles(id),
    foreign key (tag_name) references tags(display_name)
);
