package db

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"server/model"

	"github.com/go-sql-driver/mysql"
)

//CreateArticle creates a new article in the db
func CreateArticle(article *model.Article, db *sql.DB) (*model.Article, error) {
	if db == nil {
		db = conn
	}

	//create article
	result, err := db.Exec("INSERT INTO articles(url_identifier, author_uname, title, markdown, text_content) VALUES (?, ?, ?, ?, ?)",
		article.URLIdentifier, article.AuthorUname, article.Title, article.Markdown, article.TextContent)
	if err != nil {
		//detect err 1062 -- duplicate for unique field
		if driverErr, ok := err.(*mysql.MySQLError); ok && driverErr.Number == 1062 {
			dupeErr := DuplicateValueError{}
			if strings.Contains(driverErr.Message, "url_identifier") {
				dupeErr.FieldName = "url_identifier"
			}
			return nil, fmt.Errorf("could not create new article :: %w", dupeErr)
		}

		return nil, fmt.Errorf("could not create new article :: %w", err)
	}
	if id, err := result.LastInsertId(); err == nil {
		article.ID = int(id)
	} else {
		return nil, fmt.Errorf("could not retrieve ID for new article :: %w", err)
	}

	//process tags
	var tagErr error
	for _, tag := range article.Tags {
		//insert tags if new
		_, err := db.Exec(`
			INSERT INTO tags(display_name)
			SELECT ?
			WHERE NOT EXISTS (SELECT display_name FROM tags WHERE display_name = ?)`,
			tag.DisplayName, tag.DisplayName)
		if err != nil {
			err = fmt.Errorf("err creating tags for article %d :: %w", article.ID, err)
			if tagErr == nil {
				tagErr = err
			} else {
				err = fmt.Errorf("%w; %s", tagErr, err.Error())
			}
		}

		//add article_tags association
		_, err = db.Exec(`
			INSERT INTO article_tags(articleId, tag_name) VALUES (?, ?)`, article.ID, tag.DisplayName)
		if err != nil {
			err = fmt.Errorf("err creating tags for article %d :: %w", article.ID, err)
			if tagErr == nil {
				tagErr = err
			} else {
				err = fmt.Errorf("%w; %s", tagErr, err.Error())
			}
		}
	}

	return article, tagErr
}

//GetArticleByID retrieves an article by ID
func GetArticleByID(id int, db *sql.DB) (*model.Article, error) {
	if db == nil {
		db = conn
	}

	//find article
	query := `
		SELECT url_identifier, author_uname, title, markdown, articles.created_at, articles.updated_at, GROUP_CONCAT(tag_name) AS tags
		FROM articles
			LEFT JOIN article_tags ON articles.id = article_tags.articleId AND article_tags.deleted_at IS NULL
		WHERE articles.deleted_at IS NOT NULL AND articles.id = ?`

	row := db.QueryRow(query, id)

	//output
	var article *model.Article

	//column vars
	var urlIdentifier string
	var authorUname string
	var title string
	var markdown string
	var createdAt time.Time
	var updatedAt time.Time
	var tagNames sql.NullString
	if err := row.Scan(&urlIdentifier, &authorUname, &title, &markdown, &createdAt, &updatedAt, &tagNames); err != nil {
		return nil, err
	}

	//base fields
	if article == nil {
		article = &model.Article{}
		article.ID = id

		//handle time bug/oddity
		article.CreatedAt = createdAt
		article.UpdatedAt = updatedAt

		//specific fields
		article.URLIdentifier = urlIdentifier
		article.AuthorUname = authorUname
		article.Title = title
		article.Markdown = markdown
		article.Tags = make(map[string]model.Tag)
	}

	//tag(s)
	if tagNames.Valid && len(tagNames.String) > 0 {
		for _, tag := range strings.Split(tagNames.String, ",") {
			article.Tags[tag] = model.Tag{DisplayName: tag}
		}
	}

	//DEBUG
	// fmt.Printf("article: %#v\n", article)

	return article, nil
}

//GetArticleByURLIdentifier retrieves an article by its url identifier
func GetArticleByURLIdentifier(urlIdentifier string, db *sql.DB) (*model.Article, error) {
	if db == nil {
		db = conn
	}

	//find article
	query := `
		SELECT id, author_uname, title, markdown, articles.created_at, articles.updated_at, GROUP_CONCAT(tag_name) AS tags
		FROM articles
			LEFT JOIN article_tags ON articles.id = article_tags.articleId AND article_tags.deleted_at IS NULL
		WHERE articles.deleted_at IS NULL AND url_identifier = ?`

	row := db.QueryRow(query, urlIdentifier)

	//output
	var article *model.Article

	//column vars
	var id int
	var authorUname string
	var title string
	var markdown string
	var createdAt time.Time
	var updatedAt time.Time
	var tagNames sql.NullString
	if err := row.Scan(&id, &authorUname, &title, &markdown, &createdAt, &updatedAt, &tagNames); err != nil {
		return nil, err
	}

	//base fields
	if article == nil {
		article = &model.Article{}
		article.ID = id

		//handle time bug/oddity
		article.CreatedAt = createdAt
		article.UpdatedAt = updatedAt

		//specific fields
		article.URLIdentifier = urlIdentifier
		article.AuthorUname = authorUname
		article.Title = title
		article.Markdown = markdown
		article.Tags = make(map[string]model.Tag)
	}

	//tag(s)
	if tagNames.Valid && len(tagNames.String) > 0 {
		for _, tag := range strings.Split(tagNames.String, ",") {
			article.Tags[tag] = model.Tag{DisplayName: tag}
		}
	}

	//DEBUG
	// fmt.Printf("article: %#v\n", article)

	return article, nil
}

//GetArticlesHome retrieves articles for home view (paginated)
func GetArticlesHome(filterTags, filterAuthors, searchTerms []string, countPerPage, pageNumber int, db *sql.DB) ([]*model.Article, int, error) {
	if db == nil {
		db = conn
	}

	//get all articles (with filter/search conditions if any)
	queryFrom := `
		FROM (
			SELECT id, url_identifier, author_uname, title, text_content, GROUP_CONCAT(tag_name) AS tags, articles.created_at, articles.updated_at
			FROM articles
			LEFT JOIN article_tags ON articles.id = article_tags.articleId
			WHERE articles.deleted_at IS NULL AND article_tags.deleted_at IS NULL
			GROUP BY id
		) AS filtered
		%s
		ORDER BY id DESC`

	//add filter condition (if any)
	if filterTags != nil && len(filterTags) > 0 || filterAuthors != nil && len(filterAuthors) > 0 ||
		searchTerms != nil && len(searchTerms) > 0 {
		//build conditions
		filterConditions, searchConditions := make([]string, 0), make([]string, 0)
		for _, tag := range filterTags {
			filterConditions = append(filterConditions, fmt.Sprintf("filtered.tags LIKE '%%%s%%'", tag))
		}
		for _, author := range filterAuthors {
			filterConditions = append(filterConditions, fmt.Sprintf("author_uname = '%s'", author))
		}
		for _, term := range searchTerms {
			searchConditions = append(searchConditions,
				fmt.Sprintf("(title LIKE '%%%s%%') OR (text_content LIKE '%%%s%%')", term, term))
		}

		//build WHERE clause
		whereClause := strings.Builder{}
		whereClause.WriteString("WHERE ")

		//apply filters to WHERE clause (if any)
		if len(filterConditions) > 0 {
			whereClause.WriteString(strings.Join(filterConditions, " AND "))
		}

		//apply search terms to query (if any)
		if len(searchConditions) > 0 {
			if len(filterConditions) > 0 {
				whereClause.WriteString(fmt.Sprintf(" AND (%s)", strings.Join(searchConditions, "OR")))
			} else {
				whereClause.WriteString(strings.Join(searchConditions, " OR "))
			}
		}

		//apply WHERE clause to queryFrom
		queryFrom = fmt.Sprintf(queryFrom, whereClause.String())
	} else {
		queryFrom = fmt.Sprintf(queryFrom, "")
	}
	//DEBUG
	// fmt.Println("queryFrom:", queryFrom)

	//get total articles count (filtered)
	var totalArticles int
	countResult := db.QueryRow("SELECT COUNT(*) " + queryFrom)
	if err := countResult.Scan(&totalArticles); err != nil {
		return nil, 0, err
	}

	//execute paginated query (filtered)
	rows, err := db.Query("SELECT id, url_identifier, author_uname, title, LEFT(text_content, 500) AS text_content, tags, created_at, updated_at"+queryFrom+" LIMIT ?,?",
		(countPerPage * (pageNumber - 1)), countPerPage)
	if err != nil && err != sql.ErrNoRows {
		return nil, 0, err
	}

	//output
	articles := make([]*model.Article, 0)

	for rows.Next() {
		if rows.Err() != nil {
			return nil, 0, rows.Err()
		}

		//column vars
		var id int
		var urlIdentifier string
		var authorUname string
		var title string
		var textContent string
		var tags sql.NullString
		var createdAt time.Time
		var updatedAt time.Time
		if err := rows.Scan(&id, &urlIdentifier, &authorUname, &title, &textContent, &tags, &createdAt, &updatedAt); err != nil {
			return nil, 0, err
		}

		//base fields
		article := &model.Article{}
		article.ID = id
		article.CreatedAt = createdAt
		article.UpdatedAt = updatedAt

		//specific fields
		article.URLIdentifier = urlIdentifier
		article.AuthorUname = authorUname
		article.Title = title
		article.TextContent = textContent

		//handle tags
		if tags.Valid && len(tags.String) > 0 {
			article.Tags = make(map[string]model.Tag)
			for _, tag := range strings.Split(tags.String, ",") {
				article.Tags[tag] = model.Tag{DisplayName: tag}
			}
		}

		articles = append(articles, article)
	}

	return articles, totalArticles, nil
}

//UpdateArticle updates an article
func UpdateArticle(article *model.Article, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//update base entity
	_, err := db.Exec("UPDATE articles SET url_identifier = ?, title = ?, markdown = ?, text_content = ?, updated_at = current_timestamp() WHERE id = ?",
		article.URLIdentifier, article.Title, article.Markdown, article.TextContent, article.ID)
	if err != nil {
		return fmt.Errorf("could not update article %d >> %w", article.ID, err)
	}

	// -- update tags --

	//get current tags
	query := `
		SELECT tag_name
		FROM article_tags
		WHERE deleted_at IS NULL AND articleId = ?`

	rows, err := db.Query(query, article.ID)
	if err != nil {
		return fmt.Errorf("error: %v", err)
	}

	currentTags := make(map[string]struct{})

	defer rows.Close()
	for rows.Next() {
		if rows.Err() != nil {
			if err == sql.ErrNoRows {
				break
			}
			return rows.Err()
		}

		//column vars
		var displayName string
		if err := rows.Scan(&displayName); err != nil {
			if err == sql.ErrNoRows {
				break
			}
			return rows.Err()
		}

		currentTags[displayName] = struct{}{}
	}

	//delete missing tags
	tagsToDelete := make([]string, 0)
	for currentTag := range currentTags {
		if _, ok := article.Tags[currentTag]; !ok {
			tagsToDelete = append(tagsToDelete, currentTag)
		}
	}

	if len(tagsToDelete) > 0 {
		tagNamesInClause := strings.Builder{}
		tagNamesInClause.WriteRune('(')
		for idx, tag := range tagsToDelete {
			if idx > 0 {
				tagNamesInClause.WriteRune(',')
			}
			tagNamesInClause.WriteString(fmt.Sprintf("'%s'", tag))
		}
		tagNamesInClause.WriteRune(')')

		//soft delete
		_, err := db.Exec(fmt.Sprintf("UPDATE article_tags SET deleted_at = current_timestamp() WHERE deleted_at IS NULL AND tag_name IN %s", tagNamesInClause.String()))
		if err != nil {
			return fmt.Errorf("could not update removed tags for article %d >> %w", article.ID, err)
		}
	}

	//add missing/deleted tags
	tagsToAdd := make([]string, 0)
	for articleTag := range article.Tags {
		if _, ok := currentTags[articleTag]; !ok {
			tagsToAdd = append(tagsToAdd, articleTag)
		}
	}

	if len(tagsToAdd) > 0 {
		//undelete soft-deleted tags
		tagNamesInClause := strings.Builder{}
		tagNamesInClause.WriteRune('(')
		for idx, tag := range tagsToAdd {
			if idx > 0 {
				tagNamesInClause.WriteRune(',')
			}
			tagNamesInClause.WriteString(fmt.Sprintf("'%s'", tag))
		}
		tagNamesInClause.WriteRune(')')

		//un-delete
		_, err := db.Exec(fmt.Sprintf("UPDATE article_tags SET deleted_at = NULL WHERE deleted_at IS NOT NULL AND tag_name IN %s", tagNamesInClause.String()))
		if err != nil {
			return fmt.Errorf("could not update added tags for article %d >> %w", article.ID, err)
		}

		//create missing tags
		var tagErr error
		for _, tag := range tagsToAdd {
			//insert tags if new
			_, err := db.Exec(`
				INSERT INTO tags(display_name)
				SELECT ?
				WHERE NOT EXISTS (SELECT display_name FROM tags WHERE display_name = ?)`,
				tag, tag)
			if err != nil {
				err = fmt.Errorf("err creating tags for article %d >> %w", article.ID, err)
				if tagErr == nil {
					tagErr = err
				} else {
					err = fmt.Errorf("%w; %s", tagErr, err.Error())
				}
			}

			//add article_tags association
			_, err = db.Exec(`
				INSERT INTO article_tags(articleId, tag_name)
				SELECT ?, ?
				WHERE NOT EXISTS (SELECT tag_name FROM article_tags WHERE articleId = ? AND tag_name = ?)`,
				article.ID, tag, article.ID, tag)
			if err != nil {
				err = fmt.Errorf("err creating tags for article %d >> %w", article.ID, err)
				if tagErr == nil {
					tagErr = err
				} else {
					err = fmt.Errorf("%w; %s", tagErr, err.Error())
				}
			}
		}
	}

	return nil
}

//GetAllAuthors retrieves all authors of published articles
func GetAllAuthors(db *sql.DB) ([]string, error) {
	if db == nil {
		db = conn
	}

	//find article
	query := `
		SELECT DISTINCT(author_uname)
		FROM articles
		WHERE articles.deleted_at IS NULL
		ORDER BY author_uname`

	rows, err := db.Query(query)
	if err != nil {
		return nil, fmt.Errorf("error: %v", err)
	}

	//output
	authors := make([]string, 0)

	defer rows.Close()
	for rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}

		//column vars
		var authorUname string
		if err := rows.Scan(&authorUname); err != nil {
			return nil, err
		}

		authors = append(authors, authorUname)
	}

	return authors, nil
}

//GetAllTags retrieves all tags of published articles
func GetAllTags(db *sql.DB) ([]string, error) {
	if db == nil {
		db = conn
	}

	//find article
	query := `
		SELECT DISTINCT(tag_name)
		FROM article_tags
		WHERE article_tags.deleted_at IS NULL;
		ORDER BY article_tags`

	rows, err := db.Query(query)
	if err != nil {
		return nil, fmt.Errorf("error: %v", err)
	}

	//output
	tags := make([]string, 0)

	defer rows.Close()
	for rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}

		//column vars
		var tagName string
		if err := rows.Scan(&tagName); err != nil {
			return nil, err
		}

		tags = append(tags, tagName)
	}

	return tags, nil
}
