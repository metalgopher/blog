package db

import (
	"database/sql"
	"fmt"

	"server/model"
)

//InsertRoles inserts role values into db if not present
func InsertRoles(db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//run insert for each AcctRole
	for id, name := range model.GetAllAcctRoles() {
		//create new account with email and jwt
		_, err := db.Exec(`
			INSERT INTO account_roles(id, role_name)
			SELECT ?, ?
			WHERE NOT EXISTS (SELECT id from account_roles WHERE id = ?)`, id, name, id)
		if err != nil {
			return fmt.Errorf("could not add role: %w", err)
		}
	}
	return nil
}
