package db

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"server/model"
)

//GetAccountByEmail returns account by email or nil if not found
func GetAccountByEmail(email string, db *sql.DB) (*model.Account, error) {
	return getAccountByEmail(email, "email", db)
}

//GetAccountByEmailHash returns account by email hash or nil if not found
func GetAccountByEmailHash(emailHash string, db *sql.DB) (*model.Account, error) {
	return getAccountByEmail(emailHash, "hash", db)
}

//GetAccountByUsername returns account by username or nil if not found
func GetAccountByUsername(emailHash string, db *sql.DB) (*model.Account, error) {
	return getAccountByEmail(emailHash, "uname", db)
}

func getAccountByEmail(email, mode string, db *sql.DB) (*model.Account, error) {
	if db == nil {
		db = conn
	}

	query := strings.Builder{}
	query.WriteString(`
		SELECT id, email, email_hash, username, role_id, auth_code, auth_code_exp, jwt, verified_at, created_at, updated_at, deleted_at
		FROM accounts
		WHERE `)

	switch mode {
	case "email":
		query.WriteString("email")
	case "hash":
		query.WriteString("email_hash")
	case "uname":
		query.WriteString("username")
	}
	query.WriteString(" = ?")

	result := db.QueryRow(query.String(), email)
	var id sql.NullInt64
	var acctEmail sql.NullString
	var emailHash sql.NullString
	var username sql.NullString
	var role model.AcctRole
	var authCode sql.NullString
	var authCodeExp sql.NullTime
	var jwt sql.NullString
	var verifiedAt sql.NullTime
	var createdAt sql.NullTime
	var updatedAt sql.NullTime
	var deletedAt sql.NullTime
	if err := result.Scan(&id, &acctEmail, &emailHash, &username, &role, &authCode, &authCodeExp, &jwt, &verifiedAt, &createdAt, &updatedAt, &deletedAt); err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	account := &model.Account{}

	//base fields
	account.ID = int(id.Int64)
	account.CreatedAt = createdAt.Time
	account.UpdatedAt = updatedAt.Time
	if deletedAt.Valid {
		account.DeletedAt = &deletedAt.Time
	}

	//specific fields
	account.Email = acctEmail.String
	account.EmailHash = emailHash.String
	account.Username = username.String
	account.Role = model.AcctRole(role)
	if authCode.Valid {
		account.AuthCode = &authCode.String
	}
	if authCodeExp.Valid {
		account.AuthCodeExp = &authCodeExp.Time
	}
	if jwt.Valid {
		account.JWT = &jwt.String
	}
	if verifiedAt.Valid {
		account.VerifiedAt = &verifiedAt.Time
	}

	return account, nil
}

//RegisterNewAccount creates a new account with given email and jwt
func RegisterNewAccount(email, emailHash, uname, signedJWT string, role model.AcctRole, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//create new account with email and jwt
	if _, err := db.Exec("INSERT INTO accounts(email, email_hash, username, role_id, jwt) VALUES (?, ?, ?, ?, ?)", email, emailHash, uname, role, signedJWT); err != nil {
		return fmt.Errorf("could not create account: %w", err)
	}
	return nil
}

//SaveAccountAuthCode updates account with auth code and expiry
func SaveAccountAuthCode(email, authCode string, expiry time.Time, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//set authCode and authCode_exp
	if _, err := db.Exec("UPDATE accounts SET auth_code = ?, auth_code_exp = ? WHERE email = ?", authCode, expiry, email); err != nil {
		return fmt.Errorf("could not set auth code: %v", err)
	}
	return nil
}

//UpdateAccountJWT updates jwt of existing account
func UpdateAccountJWT(email, signedJWT string, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//update
	if _, err := db.Exec("UPDATE accounts SET jwt = ? WHERE email = ?", signedJWT, email); err != nil {
		return fmt.Errorf("could not update account jwt: %w", err)
	}
	return nil
}

//RegisterNewAccountVerify marks account verified and resets jwt
func RegisterNewAccountVerify(email, signedJWT string, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//mark verified and reset jwt to null
	if _, err := db.Exec("UPDATE accounts SET verified_at = current_timestamp(), jwt = null WHERE email = ?", email); err != nil {
		return fmt.Errorf("could not update account verification: %w", err)
	}
	return nil
}

//LoginAccount processes a login for given account by updating the jwt and removing auth code fields
func LoginAccount(email, signedJWT string, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//update jwt and set auth code fields to null
	if _, err := db.Exec("UPDATE accounts SET jwt = ?, auth_code = null, auth_code_exp = null WHERE email = ?", signedJWT, email); err != nil {
		return fmt.Errorf("could not process login: %w", err)
	}
	return nil
}
