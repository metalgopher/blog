package main

import (
	"server/api"
	"server/env"
)

func main() {
	env.Configure()
	api.DisplayTitle()
	api.StartupTasks()
	api.Listen()
}
