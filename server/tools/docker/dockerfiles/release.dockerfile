# Stage 1 -- cache mod deps
FROM golang:1.14-alpine AS mod

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download


# Stage 2 -- build binary
FROM golang:1.14-alpine AS builder

# install packages for binary compression
RUN apk add binutils upx tzdata

COPY --from=mod $GOCACHE $GOCACHE
COPY --from=mod $GOPATH/pkg/mod $GOPATH/pkg/mod

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .

# strip and compress the binary
RUN strip --strip-unneeded server
RUN upx server


# Stage 3 -- release
FROM alpine:3.11

RUN apk --no-cache add ca-certificates

WORKDIR /app

# create directory for test clients (for volume in dev)
RUN mkdir -p test/clients

# copy in sql scripts
COPY --from=builder /app/db/sql/* ./db/sql/

# copy in email templates
COPY --from=builder /app/email/templates/* ./email/templates/

# copy in executable
COPY --from=builder /app/server .

# copy tz data
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo

RUN adduser -D appuser

USER appuser

ENTRYPOINT ["./server"]
