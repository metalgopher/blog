# Optional first argument for environment file name within env directory, else defaults to dev.env
ENV_FILE=$1
if [ -z "$1" ]
then
    ENV_FILE="dev.env"
fi

# Optional second argument for dockerfile name within tools/docker directory, else defaults to release.dockerfile
DOCKER_FILE=$2
if [ -z "$2" ]
then
    DOCKER_FILE="release.dockerfile"
fi

docker build -t cloudbark/server:latest -f tools/docker/dockerfiles/$DOCKER_FILE . || exit 1

docker run --rm -d -p 8080:8080 --env-file env/$ENV_FILE --name cb-server cloudbark/server:latest

docker logs -f cb-server
