# Optional first argument for environment file name within env directory, else defaults to dev.env
ENV_FILE=$1
if [ -z "$1" ]
then
    ENV_FILE="dev.env"
fi

docker pull registry.gitlab.com/metalgopher/cloudbark/server:latest

docker run -d -p 8080:8080 --env-file env/$ENV_FILE --name cb-server registry.gitlab.com/metalgopher/cloudbark/server:latest

docker logs -f cb-server
